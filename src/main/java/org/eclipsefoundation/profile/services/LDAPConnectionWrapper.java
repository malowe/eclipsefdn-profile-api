/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services;

import java.util.List;

import org.eclipsefoundation.core.exception.ApplicationException;

import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;

/**
 * Defines the LDAPConnectionWrapper used to connect to LDAP and perform searches.
 */
public interface LDAPConnectionWrapper {

    /**
     * Connects to LDAP and performs a search using the provide SearchRequest entity. Returns a list of SearchResultEntry entities that
     * match the given SearchRequest parameters.
     * 
     * @param request The desired search request
     * @return A List of SearchResultEntry entities if they exist.
     */
    List<SearchResultEntry> connectAndSearch(SearchRequest request) throws ApplicationException;
}
