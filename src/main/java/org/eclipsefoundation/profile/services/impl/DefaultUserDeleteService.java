/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.profile.api.SysAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.daos.EclipsePersistenceDao;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.profile.dtos.eclipse.Friends;
import org.eclipsefoundation.profile.dtos.eclipseapi.UserDeleteRequest;
import org.eclipsefoundation.profile.helpers.UserDeleteHelper;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.mappers.DeleteRequestMapper;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.UserDeleteService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Default implementation of the UserDeleteService. Handles all UserDeleteRequest CRUD operations. Makes use of the UserDeleteHelper for
 * certain processes.
 */
@ApplicationScoped
public class DefaultUserDeleteService implements UserDeleteService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserDeleteService.class);

    // List of hosts used for UserDeleteRequest creation
    @ConfigProperty(name = "eclipse.profile.user-delete.hosts")
    Map<String, String> hosts;

    @Inject
    UserDeleteHelper userDeleteHelper;

    @RestClient
    SysAPI sysAPI;

    @Inject
    DefaultHibernateDao defaultDao;
    @Inject
    EclipsePersistenceDao eclipseDao;
    @Inject
    FilterService filters;
    @Inject
    RequestWrapper wrap;

    @Inject
    DeleteRequestMapper mapper;

    @Override
    public List<DeleteRequestData> getDeleteRequests(ProfileAPISearchParams params) {
        return getDeleteRequestsAsModels(params.mapToDBParams());
    }

    @Override
    public Optional<DeleteRequestData> getRequestById(int id) {
        List<DeleteRequestData> results = getDeleteRequestsAsModels(userDeleteHelper.buildIdParams(id));
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public Optional<UserDeleteRequest> updateDeleteRequest(DeleteRequestData src) {
        LOGGER.debug("Updating UserDeleteRequest: {}", src);
        List<UserDeleteRequest> added = defaultDao
                .add(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class)), Arrays.asList(mapper.toDTO(src, defaultDao)));
        return added.isEmpty() ? Optional.empty() : Optional.of(added.get(0));

    }

    @Override
    public void deleteDeleteRequest(int id) {
        LOGGER.debug("Deleting UserDeleteRequest with id: {}", id);
        defaultDao.delete(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class), userDeleteHelper.buildIdParams(id)));
    }

    @Override
    public List<DeleteRequestData> createDeleteRequestsForUser(AccountsProfileData user) {

        int now = (int) TimeUnit.MILLISECONDS.toSeconds(DateTimeHelper.getMillis());

        // Create a new request entity for each host
        List<UserDeleteRequest> toAdd = hosts.values().stream().map(host -> {
            UserDeleteRequest request = new UserDeleteRequest();
            request.setUid(Integer.valueOf(user.getUid()));
            request.setName(user.getName());
            request.setMail(user.getMail());
            request.setHost(host);
            request.setStatus(0);
            request.setCreated(now);
            request.setChanged(now);
            return request;
        }).collect(Collectors.toList());

        // Persist all entities in DB
        return defaultDao
                .add(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class)), toAdd)
                .stream()
                .map(mapper::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<AccountRequests> persistAccountRequestWithModLog(AccountsProfileData user) {
        try {
            // Persist a deletion AccountsRequest
            List<AccountRequests> added = eclipseDao
                    .add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)),
                            Arrays.asList(userDeleteHelper.buildDeletionAccountRequest(user.getMail(), user.getName())));

            // Track system event
            sysAPI
                    .insertSysModLog(SysModLogData
                            .builder()
                            .setLogTable("ProfileAPI")
                            .setPK1(user.getMail())
                            .setPK2(user.getUid())
                            .setLogAction("DELETEACCOUNT")
                            .setPersonId(user.getName())
                            .setModDateTime(DateTimeHelper.now())
                            .build());

            return added.isEmpty() ? Optional.empty() : Optional.of(added.get(0));
        } catch (Exception e) {
            LOGGER.error("Something went wrong while persisting AccountRequest for user: {}", user.getName(), e);
            sysAPI
                    .insertSysModLog(SysModLogData
                            .builder()
                            .setLogTable("ProfileAPI")
                            .setPK1("persistAccountRequestWithModLog")
                            .setPK2(userDeleteHelper.getBestMatchingIP())
                            .setLogAction("sql_error")
                            .setPersonId(user.getMail())
                            .setModDateTime(DateTimeHelper.now())
                            .build());
            return Optional.empty();
        }
    }

    @Override
    public void anonymizeFriendsTableForUser(String username) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(ProfileAPIParameterNames.UID.getName(), username);

        List<Friends> results = eclipseDao.get(new RDBMSQuery<>(wrap, filters.get(Friends.class), params));
        if (results != null && !results.isEmpty()) {
            Friends toUpdate = results.get(0);
            toUpdate.anonymizeEntity();
            eclipseDao.add(new RDBMSQuery<>(wrap, filters.get(Friends.class)), Arrays.asList(toUpdate));
        }
    }

    /**
     * Fetches all UserDeleteRequests from the eclipse_api DB using the given params and returns all results mapped to the DeleteRequestData
     * class.
     * 
     * @param params The desired search params.
     * @return A List of UserDeleteRequest entities if they exist
     */
    private List<DeleteRequestData> getDeleteRequestsAsModels(MultivaluedMap<String, String> params) {
        LOGGER.debug("Fetching all UserDeleteRequests with params: {}", params);
        List<UserDeleteRequest> results = defaultDao.get(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class), params));
        LOGGER.debug("Found {} result(s)", results.size());
        return results.stream().map(mapper::toModel).collect(Collectors.toList());
    }
}
