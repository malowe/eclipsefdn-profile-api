/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services.impl;

import java.security.GeneralSecurityException;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.profile.config.LDAPConnectionConfig;
import org.eclipsefoundation.profile.services.LDAPConnectionWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.util.ssl.HostNameSSLSocketVerifier;
import com.unboundid.util.ssl.SSLUtil;

@ApplicationScoped
public class DefaultLDAPConnectionWrapper implements LDAPConnectionWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLDAPConnectionWrapper.class);

    @Inject
    LDAPConnectionConfig config;

    @Override
    public List<SearchResultEntry> connectAndSearch(SearchRequest request) throws ApplicationException {
        try (LDAPConnection connection = getConnection()) {
            return connection.search(request).getSearchEntries();
        } catch (LDAPException | GeneralSecurityException e) {
            throw new ApplicationException("Error performing user search on LDAP server");
        }
    }

    /**
     * Returns the current connection. If the connection is not currently open, attempts to connect before returning.
     * 
     * @return A connected LDAPConnection entity
     * @throws LDAPException
     * @throws GeneralSecurityException
     */
    private LDAPConnection getConnection() throws LDAPException, GeneralSecurityException {
        LDAPConnectionOptions connectionOptions = new LDAPConnectionOptions();
        connectionOptions.setSSLSocketVerifier(new HostNameSSLSocketVerifier(true));
        LDAPConnection connection = new LDAPConnection(new SSLUtil().createSSLSocketFactory(), connectionOptions, config.host(),
                config.port());
        LOGGER.trace("Connection Established - Eclipse LDAP Server");
        return connection;
    }
}
