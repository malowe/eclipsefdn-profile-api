/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SplittableRandom;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Eca;
import org.eclipsefoundation.efservices.api.models.EfUser.Email;
import org.eclipsefoundation.efservices.api.models.EfUser.PublisherAgreement;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.services.ProjectService;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.full.FullPeopleProjectData;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.profile.api.MailingListAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.config.UserMetadataConfig;
import org.eclipsefoundation.profile.dtos.eclipseapi.GerritCount;
import org.eclipsefoundation.profile.helpers.ProfileHelper;
import org.eclipsefoundation.profile.helpers.UserMetadataHelper;
import org.eclipsefoundation.profile.models.GerritResponse;
import org.eclipsefoundation.profile.models.LdapResult;
import org.eclipsefoundation.profile.models.PeopleProject;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.Subscriptions;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.LDAPService;
import org.eclipsefoundation.profile.services.ProfileService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Default implementation for the ProfileService. Uses the Ef CachingService to cache all external data fetches.
 */
@Startup
@ApplicationScoped
public class DefaultProfileService implements ProfileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProfileService.class);

    private static final SplittableRandom rand = new SplittableRandom();
    private static final int RANDOM_BOUND = 100;
    private static final int RANDOM_PROBABILITY = 1;
    private static final long FUTURE_TIMEOUT = 5;

    @Inject
    UserMetadataConfig metadataConfig;

    @Inject
    LDAPService ldapService;
    @Inject
    ProjectService projectService;
    @Inject
    UserMetadataHelper metadataHelper;
    @Inject
    ProfileHelper profileHelper;

    @RestClient
    MailingListAPI mailingListAPI;

    @Inject
    ManagedExecutor executor;

    @Inject
    DefaultHibernateDao defaultDao;
    @Inject
    FilterService filters;
    @Inject
    RequestWrapper wrap;

    @Inject
    CachingService cache;

    @PostConstruct
    void init() {
        // pre-load all spec projects without loading all other entities from project service. Discarding the results
        projectService.getAllSpecProjects();
    }

    @Override
    public EfUser getProfileByUsername(String username) {
        try {
            // Search in LDAP and accounts asynchronously
            CompletableFuture<Optional<LdapResult>> ldapSearch = executor.supplyAsync(() -> ldapService.searchLdapByUsername(username));
            CompletableFuture<Optional<AccountsProfileData>> accountsSearch = executor
                    .supplyAsync(() -> profileHelper.getAccountsProfileByUsername(username));

            Optional<LdapResult> ldapResult = ldapSearch.get();
            Optional<AccountsProfileData> accountsResult = accountsSearch.get();

            return ldapResult.isEmpty() || accountsResult.isEmpty() ? null : buildEfUser(ldapResult.get(), accountsResult.get());
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new ApplicationException(String.format("Something broke while fetching profile data for user: %s", username), e,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    @Override
    public EfUser getProfileByHandle(String handle) {
        // Search in LDAP
        Optional<LdapResult> ldapResult = ldapService.searchLdapByGhHandle(handle);
        if (ldapResult.isEmpty()) {
            return null;
        }

        // Search using accounts if they exist in LDAP
        Optional<AccountsProfileData> accountsResult = profileHelper.getAccountsProfileByUsername(ldapResult.get().getUsername());
        return accountsResult.isEmpty() ? null : buildEfUser(ldapResult.get(), accountsResult.get());
    }

    @Override
    public EfUser getProfileByEmail(String email) {
        // Search for user in LDAP
        Optional<LdapResult> ldapResult = ldapService.searchLdapByEmail(email);
        if (ldapResult.isEmpty()) {
            return null;
        }

        // Search using accounts if they exist in LDAP
        Optional<AccountsProfileData> accountsResult = profileHelper.getAccountsProfileByUsername(ldapResult.get().getUsername());
        return accountsResult.isEmpty() ? null : buildEfUser(ldapResult.get(), accountsResult.get());
    }

    @Override
    public EfUser getProfileByUid(String uid) {
        // Search using accounts
        Optional<AccountsProfileData> accountsResult = profileHelper.getAccountsProfileByUid(uid);
        if (accountsResult.isEmpty()) {
            return null;
        }

        // Search for user in LDAP if they exist in accounts
        Optional<LdapResult> ldapResult = ldapService.searchLdapByUsername(accountsResult.get().getName());
        return ldapResult.isEmpty() ? null : buildEfUser(ldapResult.get(), accountsResult.get());
    }

    @Override
    public EfUser getProfileByParams(ProfileAPISearchParams params) {

        EfUser user = null;

        // Load by UID if it's present
        String uid = params.getUid().orElse(null);
        if (uid != null) {
            user = getProfileByUid(uid);
        }

        // Load by username if unable to load via uid
        String name = params.getName().orElse("");
        if (StringUtils.isNotBlank(name) && user == null) {
            user = getProfileByUsername(name);
        }

        // Load by mail if unable to load via username or uid
        String mail = params.getMail().orElse("");
        if (StringUtils.isNotBlank(mail) && user == null) {
            user = getProfileByEmail(mail);
        }

        return user;
    }

    @Override
    public Eca getEcaStatus(String username, boolean isFullProfile, List<PeopleDocumentData> userDocs,
            List<FullOrganizationContactData> orgContactData) {
        // If getting ECA for full profile, use pre-fetched data. Caching results for faster processing
        Optional<Eca> ecaResults = cache
                .get(username, new MultivaluedMapImpl<>(), Eca.class,
                        () -> isFullProfile ? getEcaStatusFromDocs(username, userDocs, orgContactData) : getEcaStatus(username))
                .getData();

        return ecaResults.isPresent() ? ecaResults.get() : Eca.builder().setSigned(false).setCanContributeSpecProject(false).build();
    }

    @Override
    public Subscriptions getSubscriptionsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching mailing-list-api for user: {}", TransformationHelper.formatLog(username));
        }
        Optional<LdapResult> user = ldapService.searchLdapByUsername(username);
        if (user.isEmpty()) {
            return null;
        }
        return Subscriptions.builder().setMailingListSubscriptions(mailingListAPI.getMailingListsByUsername(username, true)).build();
    }

    @Override
    public MultivaluedMap<String, PeopleProject> getPersonProjects(String username) {
        try {
            Optional<LdapResult> user = ldapService.searchLdapByUsername(username);
            if (user.isEmpty()) {
                return null;
            }
            // Load all PeopleProjects and spec projects asynchronously
            CompletableFuture<List<FullPeopleProjectData>> projectsSearch = executor
                    .supplyAsync(() -> profileHelper.getFullPeopleProjectsByUsername(username));
            CompletableFuture<List<Project>> specProjectSearch = executor.supplyAsync(() -> projectService.getAllSpecProjects());

            List<FullPeopleProjectData> projectsResults = projectsSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            List<Project> specProjectsResult = specProjectSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);

            // Map each project to it's projectID, filtering out inactive projects, check through specProjects if id matches
            return projectsResults.stream().collect(MultivaluedMapImpl::new, (multimap, project) -> {
                String id = project.getProject().getProjectID();
                boolean isSpecProject = specProjectsResult.stream().anyMatch(s -> s.getProjectId().equalsIgnoreCase(id));
                multimap.add(id, metadataHelper.buildPeopleProject(project, isSpecProject));
            }, MultivaluedMapImpl::putAll);

        } catch (Exception e) {
            // We don't want to fail if there is a timeout on any fdndb calls
            if (e instanceof TimeoutException) {
                return new MultivaluedMapImpl<>();
            }
            Thread.currentThread().interrupt();
            throw new ApplicationException(String.format("Something broke while fetching project data for user: %s", username), e,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    @Override
    public GerritResponse getUserGerritCount(String username) {
        Optional<AccountsProfileData> user = profileHelper.getAccountsProfileByUsername(username);
        // A missing UID means Accounts is having issues. We cannot continue with the DB query without it
        if (user.isEmpty() || !StringUtils.isNumeric(user.get().getUid())) {
            return null;
        }

        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(ProfileAPIParameterNames.UID.getName(), user.get().getUid());

        int count = 0;
        List<GerritCount> results = defaultDao.get(new RDBMSQuery<>(wrap, filters.get(GerritCount.class), params));
        if (results != null && !results.isEmpty()) {

            // If initial record exists, set the starting count
            count = results.get(0).getReviewCount();

            // Return current results if not older than 24hrs
            if (!metadataHelper.isExpiredReport(results.get(0).getReportDate())) {
                metadataHelper.buildGerritResponse(results.get(0).getReviewCount(), username);
            }
        }

        // 1% chance to fully recount
        if (rand.split().nextInt(RANDOM_BOUND) == RANDOM_PROBABILITY) {
            count = 0;
        }

        // Get all gerrit changes and add the count
        count += profileHelper.getGerritChanges(username, count).size();

        // Add updated/new record to DB
        defaultDao
                .add(new RDBMSQuery<>(wrap, filters.get(GerritCount.class)),
                        Arrays.asList(metadataHelper.buildGerritCountDto(user.get().getUid(), count)));

        final int finalCount = count;
        return metadataHelper.buildGerritResponse(finalCount, username);
    }

    /**
     * Calculates user spec project contribution and ECA status. Fetches all data from fdndb-api. The method is used when only the ECA is
     * queried.
     * 
     * @param username The given username
     * @return A user's ECA and spec project contribution status
     */
    private Eca getEcaStatus(String username) {
        // Get all non-expired docs for user
        List<PeopleDocumentData> userDocs = profileHelper.getPeopleDocumentsByUsername(username);

        // Check if signed personal docs allow for spec contribution
        boolean canContributeSpecProject = metadataHelper.userDocsAllowspecContribution(userDocs);

        // Check user org docs for spec contrubution
        if (!canContributeSpecProject) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER
                        .debug("User '{}' - Personal docs don't allow spec contribution, checking org",
                                TransformationHelper.formatLog(username));
            }
            // If the user is an org contact, check if any org documents allow for spec contribution
            List<OrganizationContactData> orgContactData = profileHelper.getOrgContactByUsername(username);
            if (!orgContactData.isEmpty()) {
                canContributeSpecProject = orgContactData
                        .stream()
                        .anyMatch(oc -> metadataHelper
                                .orgMccaAllowsSpecContribution(profileHelper.getDocumentsByOrg(Integer.toString(oc.getOrganizationID()))));
            }
        }

        // Even if they can't contribute to a spec project, their ECA may still be valid and signed
        boolean signedEca = canContributeSpecProject ? canContributeSpecProject
                : userDocs.stream().anyMatch(metadataHelper::userSignedEcaOrCla);

        return Eca.builder().setSigned(signedEca).setCanContributeSpecProject(canContributeSpecProject).build();
    }

    /**
     * Calculates user spec project contribution and ECA status. Uses data loaded from fdndb-api. The method is used when the full profile
     * is queried.
     * 
     * @param username The given username
     * @param userDocs The user's signed documents
     * @param orgContactData The user's OrgContact info
     * @return
     */
    private Eca getEcaStatusFromDocs(String username, List<PeopleDocumentData> userDocs, List<FullOrganizationContactData> orgContactData) {

        // Check if signed personal docs allow for spec contribution
        boolean canContributeSpecProject = metadataHelper.userDocsAllowspecContribution(userDocs);

        // Check user org docs for spec contrubution
        if (!canContributeSpecProject) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER
                        .debug("User '{}' - Personal docs don't allow spec contribution, checking org",
                                TransformationHelper.formatLog(username));
            }

            // If the user is an org contact, check if any org documents allow for spec contribution
            if (!orgContactData.isEmpty()) {
                canContributeSpecProject = orgContactData
                        .stream()
                        .anyMatch(oc -> metadataHelper
                                .orgMccaAllowsSpecContribution(
                                        profileHelper.getDocumentsByOrg(Integer.toString(oc.getOrganization().getOrganizationID()))));
            }
        }

        // Even if they can't contribute to a spec project, their ECA may still be valid and signed
        boolean signedEca = canContributeSpecProject ? canContributeSpecProject
                : userDocs.stream().anyMatch(metadataHelper::userSignedEcaOrCla);

        return Eca.builder().setSigned(signedEca).setCanContributeSpecProject(canContributeSpecProject).build();
    }

    /**
     * Builds a slim version of an EfUser using the given LDAP and accounts information. A slimmed down EfUser does not contain the
     * relationship URLs, eca/committer status. If the data from accounts is empty, we build the EfUser with empty fields, as the core data
     * from LDAP is still valid.
     * 
     * @param ldapResult The fetched LdapResult entity.
     * @param accountsResult An Optional containing the fetched EfUser entity form accounts.
     * @return A EfUser populated with basic profile data.
     */
    private EfUser buildSlimEfUser(LdapResult ldapResult, AccountsProfileData accountsResult) {
        return EfUser
                .builder()
                .setUid(accountsResult.getUid())
                .setName(ldapResult.getUsername())
                .setPicture(accountsResult.getPicture())
                .setMail(ldapResult.getMail())
                .setFirstName(ldapResult.getFirstName())
                .setLastName(ldapResult.getLastName())
                .setFullName(ldapResult.getFullName())
                .setPublisherAgreements(Collections.emptyMap())
                .setGithubHandle(ldapResult.getGithubId())
                .setTwitterHandle(accountsResult.getTwitterHandle())
                .setJobTitle(accountsResult.getJobTitle())
                .setWebsite(accountsResult.getWebsite())
                .setCountry(accountsResult.getCountry())
                .setBio(accountsResult.getBio())
                .setInterests(accountsResult.getInterests())
                .setWorkingGroupsInterests(accountsResult.getWorkingGroupsInterests())
                .setPublicFields(accountsResult.getPublicFields())
                .setOrg(accountsResult.getOrg())
                .build();
    }

    /**
     * Builds an EFUser entity using the given LDAP and Accounts results. Fetches from fdndb-api for additional ECA/committer status,
     * publisher agreement information, and any org data.
     * 
     * @param ldapResult The given LDAP result
     * @param accountsResult The given Accounts result
     * @return A populated EfUser entity with all relevant data.
     */
    private EfUser buildEfUser(LdapResult ldapResult, AccountsProfileData accountsResult) {
        EfUser.Builder slimUserBuilder = buildSlimEfUser(ldapResult, accountsResult).toBuilder();
        String username = ldapResult.getUsername();
        LOGGER.debug("Building EFUser with name: '{}'", username);

        try {
            // Load all PeopleDocs, OrgContactData, and PeopleProjectData
            CompletableFuture<List<PeopleDocumentData>> userDocSearch = executor
                    .supplyAsync(() -> profileHelper.getPeopleDocumentsByUsername(username));
            CompletableFuture<List<FullOrganizationContactData>> orgContactSearch = executor
                    .supplyAsync(() -> profileHelper.getFullOrgContactByUsername(username));
            CompletableFuture<List<FullPeopleProjectData>> userProjectSearch = executor
                    .supplyAsync(() -> profileHelper.getFullPeopleProjectsByUsername(username));

            // Process ECA using loaded data
            List<PeopleDocumentData> userDocResults = userDocSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            List<FullOrganizationContactData> orgContactData = orgContactSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            CompletableFuture<Eca> ecaStatus = executor.supplyAsync(() -> getEcaStatus(username, true, userDocResults, orgContactData));

            // Get PeopleData if it is already loaded
            List<FullPeopleProjectData> peopleProjectResults = userProjectSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS);
            Optional<PeopleData> loadedPerson = extractLoadedUser(orgContactData, peopleProjectResults);

            // Load committer status, email data, and PA status asyncronously
            CompletableFuture<List<Email>> emailSearch = executor.supplyAsync(() -> profileHelper.getEmailsByUsername(username));
            CompletableFuture<List<String>> altEmailSearch = executor
                    .supplyAsync(() -> profileHelper.getAlternateEmails(username, loadedPerson));
            CompletableFuture<Map<String, PublisherAgreement>> paStatus = executor
                    .supplyAsync(() -> metadataHelper.getAgreementStatus(userDocResults));

            // Check for an OrgContact record where the user is an employee and add the org name and id to the profile data
            if (!orgContactData.isEmpty()) {
                addEmploymentDataIfExists(slimUserBuilder, orgContactData);
            }

            addRelationURLs(slimUserBuilder, username);

            return slimUserBuilder
                    .setMailHistory(emailSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                    .setMailAlternate(altEmailSearch.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                    .setEca(ecaStatus.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                    .setIsCommitter(metadataHelper.userIsCommitter(peopleProjectResults))
                    .setPublisherAgreements(paStatus.get(FUTURE_TIMEOUT, TimeUnit.SECONDS))
                    .build();
        } catch (Exception e) {
            // We don't want to fail if there is a timeout on any fdndb calls, return what we've loaded
            if (e instanceof TimeoutException) {
                return slimUserBuilder.build();
            }
            Thread.currentThread().interrupt();
            throw new ApplicationException(String.format("Something broke while fetching metadata for user: %s", username), e,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    /**
     * Adds employment data to the current profile if the user is an employee of an organization. Adds the org_id and org name.
     * 
     * @param profile The current profile Builder object
     * @param orgContactData The loaded OrgContact data
     */
    private void addEmploymentDataIfExists(EfUser.Builder profile, List<FullOrganizationContactData> orgContactData) {
        Optional<FullOrganizationContactData> employeeRecord = orgContactData
                .stream()
                .filter(oc -> oc.getSysRelation().getRelation().equalsIgnoreCase("EMPLY"))
                .findFirst();

        if (employeeRecord.isPresent()) {
            profile
                    .setOrg(employeeRecord.get().getOrganization().getName1())
                    .setOrgId(employeeRecord.get().getOrganization().getOrganizationID());
        }
    }

    /**
     * Adds relationship URLs to the current profile.
     * 
     * @param profile The current profile Builder object
     * @param orgContactData The given username
     */
    private void addRelationURLs(EfUser.Builder profile, String username) {
        profile
                .setEcaUrl(metadataHelper.buildRelationshipUrl(username, "eca"))
                .setProjectsUrl(metadataHelper.buildRelationshipUrl(username, "projects"))
                .setGerritUrl(metadataHelper.buildRelationshipUrl(username, "gerrit"))
                .setMailinglistUrl(metadataHelper.buildRelationshipUrl(username, "mailing-list"))
                .setMpcFavoritesUrl(metadataConfig.url().marketplaceFavoritesUrl() + username);
    }

    /**
     * Extracts the loaded user from the FullOrgContact or FullPeopleProject fetches. This is to help eliminate duplicate calls to fdndb-api
     * when we've already received the full People entity.
     * 
     * @param contactData The FullOrgContactData for the current user if it exists
     * @param projectData The FullPeopleProjectData for the current user if it exists
     * @return An Optional containing the loaded user if it exists.
     */
    private Optional<PeopleData> extractLoadedUser(List<FullOrganizationContactData> contactData, List<FullPeopleProjectData> projectData) {
        Optional<PeopleData> loadedUser = Optional.empty();
        if (!contactData.isEmpty()) {
            loadedUser = Optional.of(contactData.get(0).getPerson());
        } else if (!projectData.isEmpty()) {
            loadedUser = Optional.of(projectData.get(0).getPerson());
        }
        return loadedUser;
    }
}
