/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.profile.dtos.eclipseapi.UserDeleteRequest;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;

/**
 * Defines the service used to perform operations related to user delete requests.
 */
public interface UserDeleteService {

    /**
     * Returns a list of DeleteRequestData entities using the given query params from the request. Maps the DTOs to models before returning
     * data.
     * 
     * @param params The BeanParam containing all the given request query params
     * @return A List of DeleteRequestData if any match the incoming parameters
     */
    List<DeleteRequestData> getDeleteRequests(ProfileAPISearchParams params);

    /**
     * Returns an Optional containing a DeleteRequestData entity from the DB that matches the given id if it exists.
     * 
     * @param id The desired request id
     * @return An Optional containing a UserDeleteRequest entity if it exists
     */
    Optional<DeleteRequestData> getRequestById(int id);

    /**
     * Persists a DeleteRequestData entity in the eclipse_api DB.
     * 
     * @param src The UserDeleteRequest entity to persist.
     * @return An Optional containing the newly persisted UserDeleteRequest if it was successful.
     */
    Optional<UserDeleteRequest> updateDeleteRequest(DeleteRequestData src);

    /**
     * Deletes a UserDeleteRequest from the DB that matches the given request id.
     * 
     * @param id The id of the request record to delete.
     */
    void deleteDeleteRequest(int id);

    /**
     * Creates a UserDeleteRequest entity for each of the valid hosts, persists it into the DB, and returns the results.
     * 
     * @param user The user tied to the delete requests
     * @return A list of newly created delete request entities if they exist
     */
    List<DeleteRequestData> createDeleteRequestsForUser(AccountsProfileData user);

    /**
     * Creates an AccountRequests entity for the given user. Tracks the SysLog event on success and failure.
     * 
     * @param user The user tied to the deletion request
     * @return An Optional containing the newly created AccountRequests entity if it exists.
     */
    Optional<AccountRequests> persistAccountRequestWithModLog(AccountsProfileData user);

    /**
     * Anonymizes the Friends table record for the given user if they have one.
     * 
     * @param username The desired user
     */
    void anonymizeFriendsTableForUser(String username);
}
