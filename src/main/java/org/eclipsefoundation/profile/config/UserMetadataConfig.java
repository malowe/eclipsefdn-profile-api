/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * ConfigMapper used to capture all metadata related configs used int his application. Contains relationship URLs as ECA validation
 * configs.
 */
@ConfigMapping(prefix = "eclipse.profile.metadata")
public interface UserMetadataConfig {

    MetadataURLDefinition url();

    EcaValidationDefinition ecaValidation();

    PublisherAgreementValidationDefinition publisherAgreementValidation();

    /**
     * ConfigMapper used to capture all profile metadata URLs. Contains the application base, MPC favorites, gerrit owner, and gerrit
     * reviewer URLs.
     */
    interface MetadataURLDefinition {
        @WithDefault("https://api.eclipse.org/account/profile/")
        String applicationBaseUrl();

        @WithDefault("https://api.eclipse.org/marketplace/favorites?name=")
        String marketplaceFavoritesUrl();

        @WithDefault("https://git.eclipse.org/r/changes?owner:")
        String gerritOwnerUrl();

        @WithDefault("https://git.eclipse.org/r/changes?reviewer:")
        String gerritReviewerUrl();
    }

    /**
     * ConfigMapper used to capture all ECA document validation parameters. Contains ECA doc version and id, ICA doc id, MCCA doc id, CLA
     * id, and IWGPA doc ids.
     */
    interface EcaValidationDefinition {
        String ecaId();

        @WithDefault("3.0,3.1")
        List<Double> ecaVersion();

        String icaId();

        String mccaId();

        List<String> iwgpaIds();

        String claId();
    }

    /**
     * ConfigMapper used to capture all publisher agreement calidation parameters. Contains the document Id.
     */
    interface PublisherAgreementValidationDefinition {
        String docId();
    }
}
