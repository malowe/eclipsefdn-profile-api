/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.config;

import io.smallrye.config.ConfigMapping;

/**
 * Loads all LDAP connection config properties.
 */
@ConfigMapping(prefix = "eclipse.profile.ldap")
public interface LDAPConnectionConfig {

    String host();

    Integer port();

    String baseDn();
}
