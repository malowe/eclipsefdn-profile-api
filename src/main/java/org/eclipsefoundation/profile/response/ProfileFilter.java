/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.response;

import java.io.IOException;
import java.util.Collections;

import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Country;
import org.eclipsefoundation.efservices.models.RequestTokenWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ProfileFilter implements ContainerResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileFilter.class);

    @Inject
    RequestTokenWrapper tokenWrap;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        // Anonymize private fields if response is an EfUser entity
        if (responseContext.getEntity() instanceof EfUser && !tokenWrap.isAuthenticated()) {
            EfUser profile = (EfUser) responseContext.getEntity();
            responseContext
                    .setEntity(profile
                            .toBuilder()
                            .setMail("")
                            .setCountry(Country.builder().setCode(null).setName(null).build())
                            .setPublicFields(Collections.emptyList())
                            .setMailAlternate(Collections.emptyList())
                            .setMailHistory(Collections.emptyList())
                            .build());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Profile anonymized for user: {}", profile.getName());
            }
        }
    }
}
