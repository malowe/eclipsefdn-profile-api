/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.daos;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;

import org.eclipsefoundation.persistence.dao.impl.BaseHibernateDao;

/**
 * DAO bound to the 'eclipse' DB. The default DAO for this application is bound
 * to the 'eclipse_api' DB.
 */
@Singleton
public class EclipsePersistenceDao extends BaseHibernateDao {

    @Named("eclipse")
    @Inject
    EntityManager em;

    @Override
    protected EntityManager getPrimaryEntityManager() {
        return em;
    }
}
