/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_DeleteRequestResult.Builder.class)
public abstract class DeleteRequestResult {

    public abstract List<DeleteRequestData> getResult();

    public abstract Pagination getPagination();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_DeleteRequestResult.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setResult(List<DeleteRequestData> result);

        public abstract Builder setPagination(Pagination pagination);

        public abstract DeleteRequestResult build();
    }


    @AutoValue
    @JsonDeserialize(builder = AutoValue_DeleteRequestResult_Pagination.Builder.class)
    public abstract static class Pagination {

        public abstract int getPage();

        public abstract int getPageSize();

        public abstract int getResultStart();

        public abstract int getResultEnd();

        public abstract int getResultSize();

        public abstract String getTotalResultSize();

        public static Builder builder() {
            return new AutoValue_DeleteRequestResult_Pagination.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setPage(int page);

            public abstract Builder setPageSize(int size);

            public abstract Builder setResultStart(int start);

            public abstract Builder setResultEnd(int end);

            public abstract Builder setResultSize(int size);

            public abstract Builder setTotalResultSize(String total);

            public abstract Pagination build();
        }
    }
}
