/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The object returned from the Gerrit relationship endpoint.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GerritResponse.Builder.class)
public abstract class GerritResponse {

    public abstract String getMergedChangesCount();

    public abstract String getGerritOwnerUrl();

    public abstract String getGerritReviewerUrl();

    public abstract String getAccountUrl();

    public static Builder builder() {
        return new AutoValue_GerritResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setMergedChangesCount(String count);

        public abstract Builder setGerritOwnerUrl(String url);

        public abstract Builder setGerritReviewerUrl(String url);

        public abstract Builder setAccountUrl(String url);

        public abstract GerritResponse build();
    }
}
