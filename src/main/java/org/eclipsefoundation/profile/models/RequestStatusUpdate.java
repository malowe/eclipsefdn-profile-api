/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Incoming request body used to update the status of a user_delete_request
 * entity.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_RequestStatusUpdate.Builder.class)
public abstract class RequestStatusUpdate {

    public abstract Optional<Integer> getStatus();

    public static Builder builder() {
        return new AutoValue_RequestStatusUpdate.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setStatus(Optional<Integer> status);

        public abstract RequestStatusUpdate build();
    }
}
