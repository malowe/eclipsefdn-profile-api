/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The result returned from an LDAP user search.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_LdapResult.Builder.class)
public abstract class LdapResult {

    @Nullable
    public abstract String getUsername();

    @Nullable
    public abstract String getMail();

    @Nullable
    public abstract String getGithubId();

    @Nullable
    public abstract String getLastName();

    @Nullable
    public abstract String getFirstName();

    @Nullable
    public abstract String getFullName();

    public static Builder builder() {
        return new AutoValue_LdapResult.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setUsername(@Nullable String uid);

        public abstract Builder setMail(@Nullable String mail);

        public abstract Builder setGithubId(@Nullable String id);

        public abstract Builder setLastName(@Nullable String name);

        public abstract Builder setFirstName(@Nullable String name);

        public abstract Builder setFullName(@Nullable String name);

        public abstract LdapResult build();
    }
}
