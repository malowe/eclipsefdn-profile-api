/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import java.util.Optional;

import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

/**
 * Incoming query params.
 */
public class ProfileAPISearchParams {

    // Used to strip host names
    private static final String HOST_MATCH_PATTERN = "https?://|www\\.";

    @QueryParam("uid")
    private Optional<String> uid;

    @QueryParam("name")
    private Optional<String> name;

    @QueryParam("mail")
    private Optional<String> mail;

    @QueryParam("host")
    private Optional<String> host;

    @QueryParam("status")
    private Optional<Integer> status;

    @QueryParam("since")
    private Optional<Integer> since;

    @QueryParam("until")
    private Optional<Integer> until;

    public ProfileAPISearchParams() {
        this.uid = Optional.empty();
        this.name = Optional.empty();
        this.mail = Optional.empty();
        this.host = Optional.empty();
        this.status = Optional.empty();
        this.since = Optional.empty();
        this.until = Optional.empty();
    }

    public Optional<String> getUid() {
        return uid;
    }

    public void setGetUid(Optional<String> uid) {
        this.uid = uid;
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<String> getMail() {
        return mail;
    }

    public void setMail(Optional<String> mail) {
        this.mail = mail;
    }

    public Optional<String> getHost() {
        return host;
    }

    public void setHost(Optional<String> host) {
        this.host = host;
    }

    public Optional<Integer> getStatus() {
        return status;
    }

    public void setStatus(Optional<Integer> status) {
        this.status = status;
    }

    public Optional<Integer> getSince() {
        return since;
    }

    public void setSince(Optional<Integer> since) {
        this.since = since;
    }

    public Optional<Integer> getUntil() {
        return until;
    }

    public void setUntil(Optional<Integer> until) {
        this.until = until;
    }

    @Override
    public String toString() {
        return "DeleteRequestSearchParams [uid=" + uid + ", name=" + name + ", mail=" + mail + ", host=" + host + ", status=" + status
                + ", since=" + since + ", until=" + until + "]";
    }

    public MultivaluedMap<String, String> mapToUserSearchCacheParams() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        uid.ifPresent(u -> params.add(ProfileAPIParameterNames.UID.getName(), u));
        name.ifPresent(n -> params.add(ProfileAPIParameterNames.NAME.getName(), n));
        mail.ifPresent(m -> params.add(ProfileAPIParameterNames.MAIL.getName(), m));
        return params;
    }

    public MultivaluedMap<String, String> mapToDBParams() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        this.uid.ifPresent(u -> params.add(ProfileAPIParameterNames.UID.getName(), u));
        this.name.ifPresent(n -> params.add(ProfileAPIParameterNames.NAME.getName(), n));
        this.mail.ifPresent(m -> params.add(ProfileAPIParameterNames.MAIL.getName(), m));
        this.host.ifPresent(h -> params.add(ProfileAPIParameterNames.HOST.getName(), h.replaceAll(HOST_MATCH_PATTERN, "")));
        this.status.ifPresent(st -> params.add(ProfileAPIParameterNames.STATUS.getName(), Integer.toString(st)));
        this.since.ifPresent(si -> params.add(ProfileAPIParameterNames.SINCE.getName(), Integer.toString(si)));
        this.until.ifPresent(un -> params.add(ProfileAPIParameterNames.UNTIL.getName(), Integer.toString(un)));
        return params;
    }
}
