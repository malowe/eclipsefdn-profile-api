/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models.mappers;

import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.eclipsefoundation.profile.dtos.eclipseapi.UserDeleteRequest;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.mapstruct.Mapper;

@Mapper(config = QuarkusMappingConfig.class)
public interface DeleteRequestMapper extends BaseEntityMapper<UserDeleteRequest, DeleteRequestData> {

}
