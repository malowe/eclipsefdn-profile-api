/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import java.time.LocalDate;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Return object for the projects relationship entity
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_PeopleProject.Builder.class)
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public abstract class PeopleProject {

    public abstract LocalDate getActiveDate();

    @Nullable
    public abstract LocalDate getInactiveDate();

    public abstract String getEditBugs();

    public abstract String getSortOrder();

    public abstract String getProjectName();

    public abstract String getUrl();

    public abstract boolean getSpecificationProject();

    public abstract Relation getRelation();

    public static Builder builder() {
        return new AutoValue_PeopleProject.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setActiveDate(LocalDate date);

        public abstract Builder setInactiveDate(@Nullable LocalDate date);

        public abstract Builder setEditBugs(String bugs);

        public abstract Builder setSortOrder(String order);

        public abstract Builder setProjectName(String name);

        public abstract Builder setUrl(String url);

        public abstract Builder setSpecificationProject(boolean project);

        public abstract Builder setRelation(Relation relation);

        public abstract PeopleProject build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_PeopleProject_Relation.Builder.class)
    public abstract static class Relation {

        public abstract String getRelation();

        public abstract String getDescription();

        public abstract String getIsActive();

        public abstract RelationTypeData getType();

        public static Builder builder() {
            return new AutoValue_PeopleProject_Relation.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setRelation(String relation);

            public abstract Builder setDescription(String desc);

            public abstract Builder setIsActive(String active);

            public abstract Builder setType(RelationTypeData type);

            public abstract Relation build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_PeopleProject_RelationTypeData.Builder.class)
    public abstract static class RelationTypeData {

        public abstract String getType();

        public abstract String getDescription();

        public static Builder builder() {
            return new AutoValue_PeopleProject_RelationTypeData.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setType(String type);

            public abstract Builder setDescription(String desc);

            public abstract RelationTypeData build();
        }
    }
}
