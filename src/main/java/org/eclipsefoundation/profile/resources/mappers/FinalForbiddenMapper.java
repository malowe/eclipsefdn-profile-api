/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources.mappers;

import java.util.Arrays;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom exception mapper class used to map 404 responses to the D7 error return format.
 * 
 */
@Provider
public class FinalForbiddenMapper implements ExceptionMapper<FinalForbiddenException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FinalForbiddenMapper.class);

    @Override
    public Response toResponse(FinalForbiddenException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return Response.status(Status.FORBIDDEN).entity(Arrays.asList(exception.getMessage())).build();
    }
}