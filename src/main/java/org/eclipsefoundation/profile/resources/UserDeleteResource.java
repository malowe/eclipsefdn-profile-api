/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import jakarta.annotation.Nullable;
import jakarta.inject.Inject;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;

import org.eclipsefoundation.core.config.PaginationConfig;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.response.PaginatedResultsFilter;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.profile.annotations.Authenticated;
import org.eclipsefoundation.profile.config.UserMetadataConfig;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.DeleteRequestResult;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.RequestStatusUpdate;
import org.eclipsefoundation.profile.services.UserDeleteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource class containing all 'user_delete_request' CRUD endpoints.
 */
@Path("account/user_delete_request")
public class UserDeleteResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDeleteResource.class);

    private static final String DELETE_REQUEST_NOT_FOUND_MSG = "User delete request entity was not found.";

    private static final String DEFAULT_PAGE = "1";

    private static final int[] VALID_DELETE_STATUSES = { 1, 2 };

    @Inject
    UserDeleteService deleteService;

    @Inject
    RequestWrapper wrap;

    @Inject
    UserMetadataConfig metadataConfig;

    @Inject
    PaginationConfig paginationConfig;

    @Inject
    PaginationHeaderService pagination;

    @Context
    UriInfo uriInfo;

    /**
     * Returns a 200 OK Response containing a {@Link DeleteRequestResult} object containing all UserDeleteRequest entities that match the
     * given params as well as pagination informatin. Will return an empty list if no matching results are found. All potential
     * ProfileAPISearchParams properties are relevant to this endpoint.
     * 
     * @param params The incoming query params. They include: uid, name, mail, host, status, since, and until
     * @return A 200 OK Response containing all UserDeleteRequest entities that match the given params.
     */
    @GET
    @Authenticated
    public Response getDeleteRequests(@BeanParam ProfileAPISearchParams params) {

        // Add default page size and page to request wrapper if not present
        if (wrap.getFirstParam(DefaultUrlParameterNames.PAGESIZE).isEmpty()) {
            wrap.setParam(DefaultUrlParameterNames.PAGESIZE, Integer.toString(paginationConfig.filter().defaultPageSize()));
        }
        if (wrap.getFirstParam(DefaultUrlParameterNames.PAGE).isEmpty()) {
            wrap.setParam(DefaultUrlParameterNames.PAGE, DEFAULT_PAGE);
        }

        // Fetch paginated delete requests and get total results
        List<DeleteRequestData> results = deleteService.getDeleteRequests(params);
        String totalResults = pagination.resolveHeadersForRequest(wrap).get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER);

        int currentPage = Integer.parseInt(wrap.getFirstParam(DefaultUrlParameterNames.PAGE).orElse(DEFAULT_PAGE));
        int pageSize = wrap.getPageSize();
        int start = (currentPage - 1) * pageSize;
        int lastPage = Integer.parseInt(totalResults) / pageSize;

        // Create all relevant links for the link header
        List<Link> links = new ArrayList<>();
        links.add(buildLinkHeader("self", "this page of results", currentPage));
        links.add(buildLinkHeader("first", "first page of results", 1));
        links.add(buildLinkHeader("last", "last page of results", lastPage));
        if (currentPage > 1) {
            links.add(buildLinkHeader("prev", "previous page of results", currentPage - 1));
        }
        if (currentPage < lastPage) {
            links.add(buildLinkHeader("next", "next page of results", currentPage + 1));
        }

        return Response
                .ok(DeleteRequestResult
                        .builder()
                        .setResult(results)
                        .setPagination(DeleteRequestResult.Pagination
                                .builder()
                                .setPage(currentPage)
                                .setPageSize(wrap.getPageSize())
                                .setResultStart(start + 1)
                                .setResultEnd(start + results.size())
                                .setResultSize(results.size())
                                .setTotalResultSize(totalResults)
                                .build())
                        .build())
                .header(HttpHeaders.LINK, flattenLinkHeader(links))
                .build();
    }

    /**
     * Returns a 200 OK Response containing the UserDeleteRequestData entity matching the given 'requestId'. Returns a 404 Not Found
     * Response if the requested UserDeleteRequestData entity is not found.
     * 
     * @param requestId The given UserDeleteRequest id.
     * @return A 200 OK Response containing the UserDeleteRequestData entity matching the given 'requestId'. A 404 Not Found Response if the
     * requested entity is not found.
     */
    @GET
    @Authenticated
    @Path("{requestId}")
    public Response getDeleteRequest(@PathParam("requestId") int requestId) {
        Optional<DeleteRequestData> result = deleteService.getRequestById(requestId);
        if (result.isEmpty()) {
            throw new NotFoundException(DELETE_REQUEST_NOT_FOUND_MSG);
        }
        return Response.ok(result.get()).build();
    }

    /**
     * Returns a 204 No Content if the requested UserDeleteRequest was successfully updated with the status from the request body. Returns a
     * 400 Bad Request Response if the incoming 'status' field is invalid or missing. Returns a 404 Not Found Response if the requested
     * entity cannot be found.
     * 
     * @param requestId The id of the request to update.
     * @param body The incoming body. Should contain single 'status' field.
     * @return A 204 No Content if the requested UserDeleteRequest was successfully updated. A 400 Bad Request Response if the incoming
     * request body's 'status' field is invalid or missing. A 404 Not Found Response if the requested entity cannot be found.
     */
    @PUT
    @Authenticated
    @Path("{requestId}")
    public Response updateDeleteRequest(@PathParam("requestId") int requestId, @Nullable RequestStatusUpdate body) {

        if (body == null || body.getStatus().isEmpty()) {
            return Response
                    .status(Status.BAD_REQUEST)
                    .entity(Arrays
                            .asList("The request could not be understood by the server due to malformed syntax. The client should not repeat the request without modifications."))
                    .build();
        }

        // Fetch the request to update
        Optional<DeleteRequestData> toUpdate = deleteService.getRequestById(requestId);
        if (toUpdate.isEmpty()) {
            throw new NotFoundException(DELETE_REQUEST_NOT_FOUND_MSG);
        }

        // Can only update status if it is changing to 1 or 2
        int status = body.getStatus().orElse(0);
        if (Arrays.stream(VALID_DELETE_STATUSES).anyMatch(val -> val == status)) {
            DeleteRequestData updated = toUpdate
                    .get()
                    .toBuilder()
                    .setStatus(Integer.toString(status))
                    .setChanged(Long.toString(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())))
                    .build();
            deleteService.updateDeleteRequest(updated);
            LOGGER.debug("Updated UserDeleteRequest: {}", toUpdate);
        }

        return Response.status(Status.NO_CONTENT).header("status", "204 No Content").build();
    }

    /**
     * Returns a 204 Response if the UserDeleteRequest matching the given id was successfully deleted. Returns a 404 Not Found Response if
     * the requested resource was not found.
     * 
     * @param requestId The id of the request to delete.
     * @return A 204 Response if the UserDeleteRequest matching the given id was successfully deleted. A 404 Not Found Response if the
     * requested resource was not found.
     */
    @DELETE
    @Authenticated
    @Path("{requestId}")
    public Response deleteDeleteRequest(@PathParam("requestId") int requestId) {
        // Request must exist before deletion
        if (deleteService.getRequestById(requestId).isEmpty()) {
            throw new NotFoundException(DELETE_REQUEST_NOT_FOUND_MSG);
        }

        deleteService.deleteDeleteRequest(requestId);
        LOGGER.debug("Deleted UserDeleteRequest with id: {}", requestId);

        return Response.status(Status.NO_CONTENT).header("status", "204 No Content").build();
    }

    /**
     * Flattens each link object into a single string. This is done since the ResponseBuilder.Link() method adds each individual Link into
     * it's own header.
     * 
     * @param linkHeaders The list of created Link objects
     * @return A single Link header string
     */
    private String flattenLinkHeader(List<Link> linkHeaders) {
        StringBuilder sb = new StringBuilder(linkHeaders.toString().length());
        for (int i = 0; i < linkHeaders.size(); ++i) {
            sb.append(linkHeaders.get(i).toString());
            // Don't add a comma separator if on the last Link
            if (i != linkHeaders.size() - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    /**
     * Builds a link header object using the given rel, title, and page number. Replaces the page number in the current request URI with the
     * appropriate page for the link.
     * 
     * @param rel The given rel for the link
     * @param title The link's title. ex: prev, first, last, self
     * @param page The page to use for the link header
     * @return Builds a link header using the given rel, page, and title.
     */
    private Link buildLinkHeader(String rel, String title, int page) {
        String uri = uriInfo.getRequestUriBuilder().replaceQueryParam(DefaultUrlParameterNames.PAGE.getName(), page).build().toString();
        return Link.fromUri(uri).rel(rel).title(title).type(MediaType.APPLICATION_JSON).build();
    }
}
