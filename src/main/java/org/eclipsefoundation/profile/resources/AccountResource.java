/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.model.CacheWrapper;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.models.RequestUserWrapper;
import org.eclipsefoundation.profile.annotations.Authenticated;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.helpers.ProfileHelper;
import org.eclipsefoundation.profile.models.DeleteRequestData;
import org.eclipsefoundation.profile.models.GerritResponse;
import org.eclipsefoundation.profile.models.PeopleProject;
import org.eclipsefoundation.profile.models.ProfileAPISearchParams;
import org.eclipsefoundation.profile.models.Subscriptions;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.ProfileService;
import org.eclipsefoundation.profile.services.UserDeleteService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Resource class containing all endpoints related to a user's EF profile (not by GH handle). Provides endpoints for user search, user
 * profile, all user metadata, and an endpoint to initiate the user_delete_request process for a user.
 */
@Path("")
@Produces({ MediaType.APPLICATION_JSON })
public class AccountResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);

    private static final String USER_NOT_FOUND_MSG = "User not found.";
    private static final String SERVER_ERROR_MSG = "Failed connection to internal service";
    private static final List<String> CONFLICT_RESPONSE = Collections.unmodifiableList(Arrays.asList("The resource already exists"));

    @Inject
    ProfileService profileService;
    @Inject
    UserDeleteService deleteService;
    @Inject
    ProfileHelper profileHelper;

    @Inject
    RequestUserWrapper requestUser;

    @Inject
    CachingService cache;

    /**
     * Returns a 200 OK Response containing the user(s) matching the desired params. Returns a 404 Not Found Response if the user cannot be
     * found.
     * 
     * This endpoint is an exception among the profile endpoints. It does not return public data if the incoming request is unauthenticated
     * or has an invalid token. It will instead deny the request. This is because the user email is a protected field and searching via this
     * field should be restricted.
     * 
     * @param params The given search params
     * @return A 200 OK Response containing the user(s) matching the desired params. A 404 Not Found Response if the user(s) cannot be
     * found.
     */
    @GET
    @Authenticated
    @Path("account/profile")
    public Response searchForUser(@BeanParam ProfileAPISearchParams params) {

        // If search params are empty, attempt to get current user
        if (params.getMail().isEmpty() && params.getName().isEmpty() && params.getUid().isEmpty()) {

            // Get username from token user data and get user profile
            String username = requestUser.getCurrentUserEfName();
            return Response.ok(Arrays.asList(getCachedProfileByUsername(username))).build();
        }

        MultivaluedMap<String, String> cacheParams = params.mapToUserSearchCacheParams();

        // Get user via provided params
        CacheWrapper<EfUser> cacheResult = cache
                .get("usersearch", cacheParams, EfUser.class, () -> profileService.getProfileByParams(params));

        Optional<Class<?>> errorType = cacheResult.getErrorType();
        if (errorType.isPresent() && errorType.get().equals(ApplicationException.class)) {
            cache.remove(ParameterizedCacheKey.builder().setClazz(EfUser.class).setId("usersearch").setParams(cacheParams).build());
            throw new ApplicationException(SERVER_ERROR_MSG);
        }

        Optional<EfUser> profile = cacheResult.getData();
        if (profile.isEmpty()) {
            LOGGER.debug("Unable to retrieve profile data with params: {}", params);
            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }

        return Response.ok(Arrays.asList(profile.get())).build();
    }

    /**
     * Returns a 200 OK Response containing the requested EfUser entity. If the request is unauthenticated, the email and country fields
     * will be anonymized. Returns a 404 Not Found Response if the user cannot be found. Returns a 500 if there is an issue with the LDAP or
     * Accounts-API connections.
     * 
     * @param username The given EF username.
     * @return A 200 OK Response containing the requested EfUser entity. Returns a 404 Not Found Response if the user cannot be found.
     */
    @GET
    @Authenticated(allowPartialResponse = true)
    @Path("account/profile/{username}")
    public Response getUserProfileByUsername(@PathParam("username") String username) {
        return Response.ok(getCachedProfileByUsername(username)).build();
    }

    /**
     * Returns a 200 OK Response containing a given user's ECA status. If the user is not found, an ECA with all false values is returned.
     * 
     * @param username The desired EF user.
     * @return An ECA entity with the desired user's status.
     */
    @GET
    @Path("account/profile/{username}/eca")
    public Response getUserEca(@PathParam("username") String username) {
        return Response.ok(profileService.getEcaStatus(username, false, Collections.emptyList(), Collections.emptyList())).build();
    }

    /**
     * Returns a 200 OK Response containing the desired user's Gerrit review count. Returns a 404 Not Found Response if the user cannot be
     * found. Returns a 500 if there was en error fetching from the Accounts-API
     * 
     * @param username The given username.
     * @return A 200 OK Response containing the desired user's Gerrit review count. A 404 Not Found Response if the user cannot be found.
     */
    @GET
    @Path("account/profile/{username}/gerrit")
    public Response getUserGerrit(@PathParam("username") String username) {
        CacheWrapper<GerritResponse> cacheResult = cache
                .get(username, new MultivaluedMapImpl<>(), GerritResponse.class, () -> profileService.getUserGerritCount(username));

        // ApplicationException is thrown when Account-API fails. Clear cache and return 500
        Optional<Class<?>> errorType = cacheResult.getErrorType();
        if (errorType.isPresent() && errorType.get().equals(ApplicationException.class)) {
            cache.remove(ParameterizedCacheKey.builder().setClazz(GerritResponse.class).setId(username).build());
            throw new ApplicationException(SERVER_ERROR_MSG);
        }

        Optional<GerritResponse> gerritData = cacheResult.getData();
        if (gerritData.isEmpty()) {
            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }

        return Response.ok(gerritData.get()).build();
    }

    /**
     * Returns a 200 OK Response containing the desired user's mailing-list subscriptions. Returns a 404 the user does not exist. Returns an
     * empty list if the user has no mailing-list subscriptions.
     * 
     * @param username The given Ef Username
     * @return A 200 OK Response containing the desired user's mailing-list subscriptions.
     */
    @GET
    @Path("account/profile/{username}/mailing-list")
    public Response getUserMailingList(@PathParam("username") String username) {
        Optional<Subscriptions> results = cache
                .get(username, new MultivaluedMapImpl<>(), Subscriptions.class, () -> profileService.getSubscriptionsByUsername(username))
                .getData();
        if (results.isEmpty()) {
            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }
        return Response.ok(results.get()).build();
    }

    /**
     * Returns a 200 OK Response containing the desired user's associated projects. Returns a 404 the user does not exist. Returns an empty
     * list if the user has no relations to projects.
     * 
     * @param username The given Ef Username
     * @return A 200 OK Response containing the desired user's mailing-list subscriptions.
     */
    @GET
    @Path("account/profile/{username}/projects")
    public Response getUserProjects(@PathParam("username") String username) {
        Optional<MultivaluedMap<String, PeopleProject>> results = cache
                .get(username, new MultivaluedMapImpl<>(), MultivaluedMap.class, () -> profileService.getPersonProjects(username))
                .getData();
        if (results.isEmpty()) {
            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }
        return Response.ok(results.get()).build();
    }

    /**
     * Initiates the user delete request process. Returns a 201 Created Response containing a list of all newly created
     * UserDeleteRequestData entities. Returns a 404 Not Found if the user cannot be found. Returns a 409 Conflict if the user has already
     * started the deletion process. Returns a 500 Internal Server Error Response if an error occurs during the persistence or if there is
     * an error fetching from Accounts-API.
     * 
     * @param username
     * @return A 201 Created Response containing a list of all newly created UserDeleteRequestData entities. A 404 Not Found if the user
     * cannot be found. Returns a 409 Conflict if the user has already started the deletion process. A 500 Internal Server Error Response if
     * an error occurs during the persistence process.
     */
    @POST
    @Authenticated
    @Path("account/profile/{username}/user_delete_request")
    public Response processUserDeleteRequest(@PathParam("username") String username) {
        Optional<AccountsProfileData> userResult = profileHelper.getAccountsProfileByUsername(username);
        if (userResult.isEmpty()) {
            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }

        AccountsProfileData user = userResult.get();

        // Check current requests via username
        ProfileAPISearchParams params = new ProfileAPISearchParams();
        params.setName(Optional.of(user.getName()));
        List<DeleteRequestData> deleteRequests = deleteService.getDeleteRequests(params);
        if (!deleteRequests.isEmpty()) {
            return Response.status(Status.CONFLICT).entity(CONFLICT_RESPONSE).build();
        }

        // Check current requests via user email
        params = new ProfileAPISearchParams();
        params.setMail(Optional.of(user.getMail()));
        deleteRequests = deleteService.getDeleteRequests(params);
        if (!deleteRequests.isEmpty()) {
            return Response.status(Status.CONFLICT).entity(CONFLICT_RESPONSE).build();
        }

        // Persist all UserDeleteRequest entities necessary
        List<DeleteRequestData> created = deleteService.createDeleteRequestsForUser(user);
        if (created == null || created.isEmpty()) {
            throw new ApplicationException("There was an error while creating delete requests",
                    Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

        // Persist deletion account request with SysEvent logging
        deleteService.persistAccountRequestWithModLog(user);

        // anonymize friends table record for user
        deleteService.anonymizeFriendsTableForUser(user.getName());

        // Return all created requests
        return Response.status(Status.CREATED).header("status", "201 Created").entity(created).build();
    }

    /**
     * Caches an EfUser entity matching the given username, returning the result if it exists. If an ApplicationException is thrown during
     * this process, the cache is cleared and a 500 is passed to the client. A 404 exception is thrown if there is no result.
     * 
     * @param username The username of the desired user.
     * @return An EfUser object if it exists and no errors were encountered.
     */
    private EfUser getCachedProfileByUsername(String username) {
        MultivaluedMap<String, String> cacheParams = new MultivaluedMapImpl<>();
        cacheParams.add(ProfileAPIParameterNames.STRATEGY.getName(), "username");
        CacheWrapper<EfUser> cacheResult = cache
                .get(username, cacheParams, EfUser.class, () -> profileService.getProfileByUsername(username));

        // ApplicationException is thrown when LDAP or accounts fails. Clear cache and return 500
        Optional<Class<?>> errorType = cacheResult.getErrorType();
        if (errorType.isPresent() && errorType.get().equals(ApplicationException.class)) {
            cache.remove(ParameterizedCacheKey.builder().setClazz(EfUser.class).setId(username).setParams(cacheParams).build());
            throw new ApplicationException(SERVER_ERROR_MSG);
        }

        // No ApplicationException means the user just doesn't exist in LDAP or accounts
        Optional<EfUser> profile = cacheResult.getData();
        if (profile.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Unable to retrieve profile data for username: {}", TransformationHelper.formatLog(username));
            }
            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }

        return profile.get();
    }
}