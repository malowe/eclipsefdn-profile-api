/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.model.CacheWrapper;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.profile.config.SlackConfig;
import org.eclipsefoundation.profile.models.SlackResponse;
import org.eclipsefoundation.profile.models.SlackResponse.SlackAttachment;
import org.eclipsefoundation.profile.services.ProfileService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Resource class for all profile fetches done via the Slack /u command.
 */
@Path("account/webhook/slack")
@Produces({ MediaType.APPLICATION_JSON })
public class SlackWebhookResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(SlackWebhookResource.class);

    private static final int DEFAULT_BUFFER_SIZE = 512;
    private static final String TOKEN_KEY = "token";
    private static final String DOMAIN_KEY = "team_domain";
    private static final String TEXT_KEY = "text";

    @Inject
    SlackConfig config;

    @Inject
    ProfileService profileService;

    @Inject
    CachingService cache;

    @POST
    @Path("u")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    public Response getUserProfile(MultivaluedMap<String, String> data) {
        LOGGER.debug("INCOMING BODY: {}", data);
        // Ensure request has valid token and team domain
        checkRequestValidity(data.getFirst(TOKEN_KEY), data.getFirst(DOMAIN_KEY));

        String text = data.getFirst(TEXT_KEY);

        // EF usernames will never have an @ symbol in them. email is assumed
        boolean isEmailSearch = text.contains("@");
        CacheWrapper<EfUser> cacheResult = cache.get(text, new MultivaluedMapImpl<>(), EfUser.class, () -> {
            if (isEmailSearch) {
                return profileService.getProfileByEmail(text);
            }
            return profileService.getProfileByUsername(text);
        });

        // ApplicationException is thrown when LDAP or accounts fails. Clear cache and return 500
        Optional<Class<?>> errorType = cacheResult.getErrorType();
        if (errorType.isPresent() && errorType.get().equals(ApplicationException.class)) {
            cache.remove(ParameterizedCacheKey.builder().setClazz(EfUser.class).setId(text).build());
            throw new ApplicationException("Failed connection to internal service");
        }

        // Build response using profile if found
        Optional<EfUser> profile = cacheResult.getData();
        if (profile.isPresent()) {
            return Response.ok(buildProfileResponse(text, profile.get())).build();
        }

        return Response.ok(buildNotFoundResponse(text)).build();
    }

    /**
     * Builds the result sent to Slack when a profile is found.
     * 
     * @param receivedValue The incoming field used to search for a profile.
     * @param profile The found profile
     * @return The constructed SlackResponse entity containing profile information.
     */
    private SlackResponse buildProfileResponse(String receivedValue, EfUser profile) {
        // Build full name title and account URL
        String accountUrl = "https://accounts.eclipse.org/user/" + profile.getUid();
        StringBuilder fullNameTitle = new StringBuilder(profile.getFullName());
        if (StringUtils.isNotBlank(profile.getOrg())) {
            fullNameTitle.append(" (");
            fullNameTitle.append(profile.getOrg());
            fullNameTitle.append(")");
        }

        return SlackResponse
                .builder()
                .setResponseType(config.responseType())
                .setText(String.format("An Eclipse account was found with *%s* %s", receivedValue, accountUrl))
                .setAttachments(Arrays
                        .asList(SlackAttachment
                                .builder()
                                .setTitle(fullNameTitle.toString())
                                .setTitleLink(accountUrl)
                                .setText(buildProfileString(profile))
                                .build()))
                .build();
    }

    /**
     * Constructs a Markdown formatted string containing all relevant profile information.
     * 
     * @param profile The found user profile
     * @return A Markdown formatted string containing profile information
     */
    private String buildProfileString(EfUser profile) {
        StringBuilder sb = new StringBuilder(DEFAULT_BUFFER_SIZE);
        sb.append("*ECA Status:* ");
        sb.append(profile.getEca() != null && profile.getEca().getSigned() ? "Valid" : "Invalid");
        sb.append("\n*Name:* ");
        sb.append(profile.getName());
        sb.append("\n*Mail:* ");
        sb.append(profile.getMail());
        sb.append("\n*Committer:* ");
        sb.append(Boolean.TRUE.equals(profile.getIsCommitter()) ? "Yes" : "No");
        if (StringUtils.isNotBlank(profile.getGithubHandle())) {
            sb.append("\n*Github handle:* https://github.com/");
            sb.append(profile.getGithubHandle());
        }
        if (StringUtils.isNotBlank(profile.getTwitterHandle())) {
            sb.append("\n*Twitter handle:* https://twitter.com/");
            sb.append(profile.getTwitterHandle());
        }
        if (StringUtils.isNotBlank(profile.getJobTitle())) {
            sb.append("\n*Job title:* ");
            sb.append(profile.getJobTitle());
        }
        if (StringUtils.isNotBlank(profile.getWebsite())) {
            sb.append("\n*Website:* ");
            sb.append(profile.getWebsite());
        }
        if (StringUtils.isNotBlank(profile.getCountry().getName())) {
            sb.append("\n*Country:* ");
            sb.append(profile.getCountry().getName());
        }
        if (StringUtils.isNotBlank(profile.getBio())) {
            sb.append("\n*Bio:* ");
            sb.append(profile.getBio());
        }
        if (!profile.getInterests().isEmpty()) {
            sb.append("\n*Interests:* ");
            sb.append(String.join(", ", profile.getInterests()));
        }
        return sb.toString();
    }

    /**
     * Builds the result sent to Slack when a profile cannot be found.
     * 
     * @param receivedValue The incoming field used to search for a profile.
     * @return The constructed SlackResponse entity containing the not found message.
     */
    private SlackResponse buildNotFoundResponse(String receivedValue) {
        return SlackResponse
                .builder()
                .setResponseType(config.responseType())
                .setText(String.format("An Eclipse account was not found with *%s*", receivedValue))
                .setAttachments(Collections.emptyList())
                .build();
    }

    /**
     * Checks the validity of incoming requests by matching the token and team domain against the configured valid values. Throws a 403
     * Forbidden if either of them do not match.
     * 
     * @param token The incoming token
     * @param teamDomain The incomin team_domain
     */
    private void checkRequestValidity(String token, String teamDomain) {
        if (StringUtils.isBlank(token) || !token.equals(config.validToken())) {
            throw new FinalForbiddenException("Invalid token");
        }
        if (StringUtils.isBlank(teamDomain) || !teamDomain.equals(config.validTeamDomain())) {
            throw new FinalForbiddenException("Invalid team domain");
        }
    }
}