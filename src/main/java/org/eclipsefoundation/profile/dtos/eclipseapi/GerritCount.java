/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.dtos.eclipseapi;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * DTO for the gerrit_review_count table.
 */
@Entity
@Table(name = "api_eclipse_api_gerrit_review_count")
public class GerritCount extends BareNode {
    public static final DtoTable TABLE = new DtoTable(GerritCount.class, "grt");

    @Id
    private int uid;
    private int reviewCount;
    private int reportDate;

    @JsonIgnore
    @Override
    public Object getId() {
        return getUid();
    }

    public int getUid() {
        return this.uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getReviewCount() {
        return this.reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public int getReportDate() {
        return this.reportDate;
    }

    public void setReportDate(int reportDate) {
        this.reportDate = reportDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(uid, reviewCount, reportDate);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        GerritCount other = (GerritCount) obj;
        return Objects.equals(uid, other.uid) && Objects.equals(reviewCount, other.reviewCount)
                && Objects.equals(reportDate, other.reportDate);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GerritCount [uid=");
        builder.append(uid);
        builder.append(", review_count=");
        builder.append(reviewCount);
        builder.append(", report_date=");
        builder.append(reportDate);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class GerritCountFilter implements DtoFilter<GerritCount> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            if (isRoot) {
                String uid = params.getFirst(ProfileAPIParameterNames.UID.getName());
                if (uid != null) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                                    new Object[] { Integer.valueOf(uid) }));
                }

                String reviewCount = params.getFirst(ProfileAPIParameterNames.REVIEW_COUNT.getName());
                if (reviewCount != null) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".reviewCount = ?",
                                    new Object[] { Integer.valueOf(reviewCount) }));
                }

                String reportDate = params.getFirst(ProfileAPIParameterNames.REPORT_DATE.getName());
                if (reportDate != null) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".reportDate = ?",
                                    new Object[] { Integer.valueOf(reportDate) }));
                }
            }

            return statement;
        }

        @Override
        public Class<GerritCount> getType() {
            return GerritCount.class;
        }
    }
}
