/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.dtos.eclipseapi;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;

/**
 * DTO for the user_delete_request table. Used to track user deltion requests.
 */
@Entity
@Table(name = "api_eclipse_api_user_delete_request")
public class UserDeleteRequest extends BareNode {
    public static final DtoTable TABLE = new DtoTable(UserDeleteRequest.class, "udr");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int uid;
    private String name;
    private String mail;
    private String host;
    private int status;
    private int created;
    private int changed;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getChanged() {
        return changed;
    }

    public void setChanged(int changed) {
        this.changed = changed;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, uid, name, mail, host, status, changed, created);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDeleteRequest other = (UserDeleteRequest) obj;
        return Objects.equals(id, other.id) && Objects.equals(uid, other.uid)
                && Objects.equals(name, other.name) && Objects.equals(mail, other.mail)
                && Objects.equals(host, other.host) && Objects.equals(status, other.status)
                && Objects.equals(changed, other.changed) && Objects.equals(created, other.created);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UserDeleteRequest [id=");
        builder.append(id);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", mail=");
        builder.append(mail);
        builder.append(", host=");
        builder.append(host);
        builder.append(", status=");
        builder.append(status);
        builder.append(", changed=");
        builder.append(changed);
        builder.append(", created=");
        builder.append(created);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class UserDeleteRequestFilter implements DtoFilter<UserDeleteRequest> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (StringUtils.isNumeric(id)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                        new Object[] { Long.valueOf(id) }));
            }

            String uid = params.getFirst(ProfileAPIParameterNames.UID.getName());
            if (StringUtils.isNumeric(uid)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                        new Object[] { Integer.valueOf(uid) }));
            }

            String status = params.getFirst(ProfileAPIParameterNames.STATUS.getName());
            if (StringUtils.isNumeric(status)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".status = ?",
                        new Object[] { Integer.valueOf(status) }));
            }

            String name = params.getFirst(ProfileAPIParameterNames.NAME.getName());
            if (StringUtils.isNotBlank(name)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".name = ?",
                        new Object[] { name }));
            }

            String mail = params.getFirst(ProfileAPIParameterNames.MAIL.getName());
            if (StringUtils.isNotBlank(mail)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".mail = ?",
                        new Object[] { mail }));
            }

            String host = params.getFirst(ProfileAPIParameterNames.HOST.getName());
            if (StringUtils.isNotBlank(host)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".host = ?",
                        new Object[] { host }));
            }

            String since = params.getFirst(ProfileAPIParameterNames.SINCE.getName());
            String until = params.getFirst(ProfileAPIParameterNames.UNTIL.getName());

            if (StringUtils.isNumeric(since) && StringUtils.isNumeric(until)) {
                statement.addClause(
                        new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".created BETWEEN ? AND ?",
                                new Object[] { Integer.valueOf(since), Integer.valueOf(until) }));
            } else if (StringUtils.isNumeric(since)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".created >= ?",
                        new Object[] { Integer.valueOf(since) }));
            } else if (StringUtils.isNumeric(until)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".created <= ?",
                        new Object[] { Integer.valueOf(until) }));
            }

            return statement;
        }

        @Override
        public Class<UserDeleteRequest> getType() {
            return UserDeleteRequest.class;
        }
    }
}
