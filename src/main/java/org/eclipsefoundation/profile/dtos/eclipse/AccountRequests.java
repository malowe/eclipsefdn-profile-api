/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.dtos.eclipse;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * DTO for the account_requests table. Used as part of the user deletion request
 * process.
 */
@Entity
@Table(name = "account_requests")
public class AccountRequests extends BareNode {
    public static final DtoTable TABLE = new DtoTable(AccountRequests.class, "acrq");

    @Id
    private AccountRequestsCompositeID compositeID;
    private String newEmail;
    private String fname;
    private String lname;
    private String password;
    private String ip;
    private String token;
    private String personId;

    @Override
    @JsonIgnore
    public Object getId() {
        return getCompositeID();
    }

    public AccountRequestsCompositeID getCompositeID() {
        return compositeID;
    }

    public void setCompositeID(AccountRequestsCompositeID compositeID) {
        this.compositeID = compositeID;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(compositeID, newEmail, fname, lname, password, ip, token, personId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountRequests other = (AccountRequests) obj;
        return Objects.equals(compositeID, other.compositeID) && Objects.equals(newEmail, other.newEmail)
                && Objects.equals(fname, other.fname) && Objects.equals(lname, other.lname)
                && Objects.equals(password, other.password) && Objects.equals(ip, other.ip)
                && Objects.equals(token, other.token) && Objects.equals(personId, other.personId);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AccountRequests [email=");
        builder.append(compositeID.getEmail());
        builder.append(", new_email=");
        builder.append(newEmail);
        builder.append(", fname=");
        builder.append(fname);
        builder.append(", lname=");
        builder.append(lname);
        builder.append(", password=");
        builder.append(password);
        builder.append(", ip=");
        builder.append(ip);
        builder.append(", reqWhen=");
        builder.append(compositeID.getReqWhen());
        builder.append(", token=");
        builder.append(token);
        builder.append(", person_id=");
        builder.append(personId);
        builder.append("]");
        return builder.toString();
    }

    @Embeddable
    public static class AccountRequestsCompositeID implements Serializable {
        private static final long serialVersionUID = 1L;

        private String email;
        private Date reqWhen;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Date getReqWhen() {
            return reqWhen;
        }

        public void setReqWhen(Date reqWhen) {
            this.reqWhen = reqWhen;
        }

        @Override
        public int hashCode() {
            return Objects.hash(email, reqWhen);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            AccountRequestsCompositeID other = (AccountRequestsCompositeID) obj;
            return Objects.equals(email, other.email) && Objects.equals(reqWhen, other.reqWhen);
        }
    }

    @Singleton
    public static class AccountRequestsFilter implements DtoFilter<AccountRequests> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);
            if (isRoot) {
                String mail = params.getFirst(ProfileAPIParameterNames.MAIL.getName());
                if (mail != null) {
                    statement
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeID.email = ?",
                                    new Object[] { mail }));
                }
            }

            String reqDate = params.getFirst(ProfileAPIParameterNames.REQ_TIME.getName());
            if (reqDate != null) {
                statement.addClause(
                        new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeID.reqWhen = ?",
                                new Object[] { reqDate }));
            }

            return statement;
        }

        @Override
        public Class<AccountRequests> getType() {
            return AccountRequests.class;
        }
    }
}
