/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import jakarta.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

/**
 * All parameters used by this application.
 */
@Singleton
public final class ProfileAPIParameterNames implements UrlParameterNamespace {

    public static final UrlParameter FRIEND_ID = new UrlParameter("friend_id");
    public static final UrlParameter UID = new UrlParameter("uid");
    public static final UrlParameter MAIL = new UrlParameter("mail");
    public static final UrlParameter REVIEW_COUNT = new UrlParameter("review_count");
    public static final UrlParameter REPORT_DATE = new UrlParameter("report_date");
    public static final UrlParameter STRATEGY = new UrlParameter("strategy");
    public static final UrlParameter NAME = new UrlParameter("name");
    public static final UrlParameter HOST = new UrlParameter("host");
    public static final UrlParameter STATUS = new UrlParameter("status");
    public static final UrlParameter SINCE = new UrlParameter("since");
    public static final UrlParameter UNTIL = new UrlParameter("until");
    public static final UrlParameter REQ_TIME = new UrlParameter("req_time");
    public static final UrlParameter CURSOR = new UrlParameter("cursor");

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(
                    Arrays.asList(FRIEND_ID, UID, MAIL, REVIEW_COUNT, REPORT_DATE, STRATEGY, NAME, HOST, STATUS, SINCE,
                            UNTIL, REQ_TIME, CURSOR));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}
