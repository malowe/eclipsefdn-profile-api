/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.namespace;

import java.util.stream.Stream;

/**
 * Enum representing an EF user's relation to a project. Includes a description
 * value.
 */
public enum RelationType {
    CO("Organization Contact"),
    ME("Organization - Member"),
    GE("General"),
    ID("Person - External ID"),
    PR("Person - Project");

    public final String description;

    private RelationType(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name();
    }

    public static RelationType getType(String relation) {
        return Stream.of(values()).filter(r -> r.getName().equalsIgnoreCase(relation)).findFirst().orElse(null);
    }
}
