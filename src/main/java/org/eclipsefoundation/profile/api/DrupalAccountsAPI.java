/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.jboss.resteasy.util.HttpHeaderNames;

/**
 * Rest client for accounts.eclipse.org. Used to fetch account data via username and uid
 */
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "accounts-profile-api")
public interface DrupalAccountsAPI {

    /**
     * Fetches accounts profile data via username.
     * 
     * @param username The given username
     * @param bearerToken the auth token required for this request
     * @return An AccountsProfileData entity if it exists
     */
    @GET
    @Path("{username}")
    AccountsProfileData getAccountsProfileByUsername(@PathParam("username") String username,
            @HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearerToken);

    /**
     * Fetches accounts profile data via uid. authentication will be removed once Accounts provides an authenticated way to fetch this data
     * via uid.
     * 
     * @param username The given username
     * @param bearerToken the auth token required for this request
     * @return A list of AccountsProfileData entities if they exist
     */
    @GET
    List<AccountsProfileData> getAccountsProfileByUid(@QueryParam("uid") String uid,
            @HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearerToken);
}
