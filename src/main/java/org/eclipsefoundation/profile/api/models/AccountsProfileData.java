/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api.models;

import java.util.List;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.efservices.api.models.EfUser.Country;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The data returned from the accounts-API.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_AccountsProfileData.Builder.class)
public abstract class AccountsProfileData {

    public abstract String getUid();

    public abstract String getMail();

    public abstract String getName();

    public abstract String getPicture();

    public abstract String getFirstName();

    public abstract String getLastName();

    public abstract String getFullName();

    public abstract String getJobTitle();

    public abstract String getWebsite();

    public abstract Country getCountry();

    @Nullable
    public abstract String getBio();

    public abstract List<String> getInterests();

    @Nullable
    public abstract List<String> getWorkingGroupsInterests();

    @Nullable
    public abstract List<String> getPublicFields();

    @Nullable
    public abstract String getOrg();

    public abstract String getTwitterHandle();

    public static Builder builder() {
        return new AutoValue_AccountsProfileData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setUid(String uid);

        public abstract Builder setMail(String mail);

        public abstract Builder setName(String name);

        public abstract Builder setPicture(String picture);

        public abstract Builder setFirstName(String fName);

        public abstract Builder setLastName(String lName);

        public abstract Builder setFullName(String name);

        public abstract Builder setJobTitle(String title);

        public abstract Builder setWebsite(String website);

        public abstract Builder setCountry(Country country);

        public abstract Builder setBio(@Nullable String bio);

        public abstract Builder setInterests(List<String> interests);

        public abstract Builder setWorkingGroupsInterests(@Nullable List<String> interests);

        public abstract Builder setPublicFields(@Nullable List<String> fields);

        public abstract Builder setOrg(@Nullable String org);

        public abstract Builder setTwitterHandle(String handle); 

        public abstract AccountsProfileData build();
    }
}
