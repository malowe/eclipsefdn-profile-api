/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * FoundationDb-API binding for '/organizations'
 */
@Path("/organizations")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "fdndb-api")
public interface OrganizationAPI {

    /**
     * Fetches all non-expired Org Docs for a given orgId
     * 
     * @param orgId The given org id
     * @param isNotExpired The not-expired flag
     * @returnA list of OrganizationDocumentData if it exists
     */
    @GET
    @Path("{orgId}/documents")
    @RolesAllowed("fdb_read_organization_documents")
    List<OrganizationDocumentData> getOrganizationDocuments(@PathParam("orgId") String orgId,
            @QueryParam("is_not_expired") boolean isNotExpired);

    /**
     * Fetches all Org Contact data for the given user
     * 
     * @param username The given username
     * @return A list of OrganizationContactData if it exists
     */
    @GET
    @Path("/contacts")
    @RolesAllowed("fdb_read_organization_employment")
    List<OrganizationContactData> getOrgContacts(@QueryParam("personID") String username);

    /**
     * Fetches all FullOrg contact data for the given user.
     * 
     * @param username The given username
     * @return A list of FullOrganizationContactData if it exists
     */
    @GET
    @Path("/contacts/full")
    @RolesAllowed({ "fdb_read_organization_employment", "fdb_read_organization", "fdb_read_people" })
    List<FullOrganizationContactData> getFullOrgContacts(@QueryParam("personID") String username);
}
