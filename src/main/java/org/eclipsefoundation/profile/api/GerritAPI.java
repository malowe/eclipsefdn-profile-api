/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * Gerrit API binding.
 */
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "gerrit-api")
public interface GerritAPI {

    /**
     * Gets all changes after a specific start point given the query.
     * 
     * @param query The given query. ex: '?q=reviewer:test@email.com status:merged'
     * @param start The starting cursor position for results
     * @return A JSON string containing all change information.
     */
    @GET
    @Path("/r/changes/")
    String getGerritChanges(@QueryParam("q") String query, @QueryParam("start") int start);
}
