/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.profile.api.models.MailingListData;

/**
 * Eclipse API binding
 */
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "eclipse-api")
public interface MailingListAPI {

    /**
     * Gets all mailing-list subscriptions for the given user.
     * 
     * @param username The given username.
     * @return A List of MailingListData entities if they exist.
     */
    @GET
    @Path("/foundation/mailing-list")
    List<MailingListData> getMailingListsByUsername(@QueryParam("username") String username,
            @QueryParam("is_subscribable") Boolean isSubscribable);
}
