/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;

import io.quarkus.oidc.client.filter.OidcClientFilter;

/**
 * Fdndb-api binding for System resources. '/sys'
 */
@Path("sys")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
@ApplicationScoped
public interface SysAPI {

    /**
     * Persists SysModLogData entities in foundationDB. Returns a list of
     * created/updated results if the operation was successful.
     * 
     * @param src The new/updated ModLog
     * @return A List or new/updated SysModLogData entities on success.
     */
    @PUT
    @Path("mod_logs")
    @RolesAllowed("fdb_write_sys_modlog")
    List<SysModLogData> insertSysModLog(SysModLogData src);
}