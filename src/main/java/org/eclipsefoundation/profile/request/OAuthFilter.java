/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.request;

import java.io.IOException;
import java.lang.reflect.Method;

import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.core.config.OAuth2SecurityConfig;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.efservices.namespace.RequestContextPropertyNames;
import org.eclipsefoundation.efservices.services.DrupalOAuthService;
import org.eclipsefoundation.profile.annotations.Authenticated;
import org.jboss.resteasy.core.interception.jaxrs.PostMatchContainerRequestContext;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter is used to validate the incoming Bearer tokens. Sets the current token user in the request context for use downstream.
 */
@Provider
public class OAuthFilter implements ContainerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(OAuthFilter.class);

    @Inject
    Instance<OAuth2SecurityConfig> config;

    @Inject
    DrupalOAuthService oauthService;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (Boolean.TRUE.equals(config.get().filter().enabled())) {

            // Get the authentication annotation if it exists
            Method m = ((PostMatchContainerRequestContext) requestContext).getResourceMethod().getMethod();
            Authenticated authenticated = m.getAnnotation(Authenticated.class);

            if (authenticated != null) {
                try {
                    String token = DrupalAuthHelper.stripBearerToken(requestContext.getHeaderString(HttpHeaderNames.AUTHORIZATION));

                    DrupalOAuthData tokenStatus = oauthService
                            .validateTokenStatus(token, config.get().filter().validScopes(), config.get().filter().validClientIds());
                    if (tokenStatus != null) {
                        requestContext.setProperty(RequestContextPropertyNames.TOKEN_STATUS, tokenStatus);

                        if (tokenStatus.getUserId() != null) {
                            // Fetch user data from token and set in context
                            LOGGER.debug("Fetching user info for token with uid: {}", tokenStatus.getUserId());
                            requestContext.setProperty(RequestContextPropertyNames.TOKEN_USER, oauthService.getTokenUserInfo(token));
                        }
                    }
                } catch (Exception e) {
                    // We want to prevent this from reaching user on profile queries.
                    LOGGER.debug("Invalid authentication", e);

                    // If the endpoint is authenticated and doesn't have a valid token, we deny the request
                    if (!authenticated.allowPartialResponse()) {
                        throw e;
                    }
                }
            }

        }
    }
}
