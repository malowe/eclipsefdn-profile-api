/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.helpers;

import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.eclipsefoundation.efservices.api.models.EfUser.PublisherAgreement;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.ProjectData;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.foundationdb.client.model.full.FullPeopleProjectData;
import org.eclipsefoundation.profile.config.UserMetadataConfig;
import org.eclipsefoundation.profile.dtos.eclipseapi.GerritCount;
import org.eclipsefoundation.profile.models.GerritResponse;
import org.eclipsefoundation.profile.models.PeopleProject;
import org.eclipsefoundation.profile.models.PeopleProject.Relation;
import org.eclipsefoundation.profile.models.PeopleProject.RelationTypeData;
import org.eclipsefoundation.profile.namespace.RelationType;

/**
 * A helper class used to perform various checks and build various entities used while handling user metadata.
 */
@ApplicationScoped
public final class UserMetadataHelper {

    @Inject
    UserMetadataConfig metadataConfig;

    /**
     * Builds a GerritCount DTO using the user uid and current change count.
     * 
     * @param uid The user's uid
     * @param count The current change count
     * @return A GerritCount DTO
     */
    public GerritCount buildGerritCountDto(String uid, int count) {
        GerritCount out = new GerritCount();
        out.setUid(Integer.valueOf(uid));
        out.setReviewCount(count);
        out.setReportDate((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        return out;
    }

    /**
     * Builds the return object for /projects from the full PeopleProjectData returned from fdndb-api.
     * 
     * @param fullData The full PeopleProjectData enitity
     * @param isSpecProject Flag determining whether this is a specification project
     * @return A mapped PeopleProject object.
     */
    public PeopleProject buildPeopleProject(FullPeopleProjectData fullData, boolean isSpecProject) {
        ProjectData project = fullData.getProject();
        SysRelationData relation = fullData.getRelation();

        return PeopleProject
                .builder()
                .setActiveDate(fullData.getActiveDate().toInstant().atZone(ZoneId.of("Etc/UTC")).toLocalDate())
                .setInactiveDate(fullData.getInactiveDate() == null ? null
                        : fullData.getInactiveDate().toInstant().atZone(ZoneId.of("Etc/UTC")).toLocalDate())
                .setEditBugs(fullData.getEditBugs() ? "1" : "0")
                .setSortOrder(project.getSortOrder() == 0 ? "" : String.valueOf(project.getSortOrder()))
                .setProjectName(project.getName())
                .setUrl(project.getUrlIndex())
                .setRelation(Relation
                        .builder()
                        .setRelation(relation.getRelation())
                        .setDescription(relation.getDescription())
                        .setIsActive(relation.isActive() ? "1" : "0")
                        .setType(buildRelationType(relation.getType()))
                        .build())
                .setSpecificationProject(isSpecProject)
                .build();
    }

    /**
     * Checks if the report date is older than 24hrs long. True if older than 24hrs, false if not.
     * 
     * @param reportDate The time since epoch of the the last report
     * @return True if older than 24hrs, false if not.
     */
    public boolean isExpiredReport(int reportDate) {
        return (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - reportDate >= TimeUnit.DAYS.toSeconds(1);
    }

    /**
     * Checks the list of signed documents for the user and validates whether they have signed a publisher agreement. Returns a populated
     * map if they have a signed agreement.
     * 
     * @param docs The documents signed by the user
     * @return A populated map with open-vsx PA version if agreement is signed, otherwise an empty map.
     */
    public Map<String, PublisherAgreement> getAgreementStatus(List<PeopleDocumentData> docs) {
        // Filter for open-vsx agreement
        Optional<PeopleDocumentData> agreement = docs
                .stream()
                .filter(d -> d.getDocumentID().equalsIgnoreCase(metadataConfig.publisherAgreementValidation().docId()))
                .findFirst();

        return agreement.isPresent()
                ? Map.of("open-vsx", PublisherAgreement.builder().setVersion(Integer.toString((int) agreement.get().getVersion())).build())
                : Collections.emptyMap();
    }

    /**
     * Builds a GerritResponse object given the desired count, and username. Constructs owner, reviewer, and account URLs.
     * 
     * @param count The user review count.
     * @param username The username.
     * @return A fully populated GerritResponse with all related URLs.
     */
    public GerritResponse buildGerritResponse(int count, String username) {
        return GerritResponse
                .builder()
                .setMergedChangesCount(Integer.toString(count))
                .setGerritOwnerUrl(metadataConfig.url().gerritOwnerUrl() + username)
                .setGerritReviewerUrl(metadataConfig.url().gerritReviewerUrl() + username)
                .setAccountUrl(metadataConfig.url().applicationBaseUrl() + "account/profile/" + username)
                .build();
    }

    /**
     * Builds a relation URL for a given user. ex: api.eclipse.org/account/profile/{username}/{resource}
     * 
     * @param username The given username
     * @param resource The given URL resource
     * @return A URL pointed at a relation resource
     */
    public String buildRelationshipUrl(String username, String resource) {
        return metadataConfig.url().applicationBaseUrl() + "account/profile/" + username + "/" + resource;
    }

    /**
     * Uses a list of active projects for a given user and eturns true if the user has a committer status on any project.
     * 
     * @param projectRelations The list of user project relations
     * @return True if user has committer status. False if not.
     */
    public boolean userIsCommitter(List<FullPeopleProjectData> projectRelations) {
        return projectRelations.stream().anyMatch(p -> p.getRelation().getRelation().equalsIgnoreCase("cm"));
    }

    public boolean userDocsAllowspecContribution(List<PeopleDocumentData> docs) {
        return docs
                .stream()
                .anyMatch(d -> userEcaAllowsSpecContribution(d) || userIcaAllowsSpecContribution(d) || userIwgpaAllowsSpecContribution(d));
    }

    /**
     * Checks the list of signed documents for the user's organization and validates whether they are covered by the ECA.
     * 
     * @param docs The documents signed by the org
     * @return True if any match, false if there's no match
     */
    public boolean orgMccaAllowsSpecContribution(List<OrganizationDocumentData> docs) {
        return docs.stream().anyMatch(d -> d.getDocumentID().equalsIgnoreCase(metadataConfig.ecaValidation().mccaId()));
    }

    /**
     * Checks if the current PeopleDocument is an ECA or CLA. Used to validate ECA status.
     * 
     * @param doc The current PeopleDocumentData to check
     * @return True if the document is an ECA or CLA
     */
    public boolean userSignedEcaOrCla(PeopleDocumentData doc) {
        return doc.getDocumentID().equals(metadataConfig.ecaValidation().claId())
                || doc.getDocumentID().equals(metadataConfig.ecaValidation().ecaId());
    }

    /**
     * Checks if the current PeopleDocument is one of the valid individual WG participation agreements that allows for spec project
     * contribution.
     * 
     * @param doc The current PeopleDocumentData to check
     * @return True if the document is one of the valid iwgpas
     */
    private boolean userIwgpaAllowsSpecContribution(PeopleDocumentData doc) {
        return metadataConfig.ecaValidation().iwgpaIds().contains(doc.getDocumentID());
    }

    /**
     * Checks if the current PeopleDocument is an Individual Contribution Agreement that allows for spec project contribution.
     * 
     * @param doc The current PeopleDocumentData to check
     * @return True if the document is an ICA
     */
    private boolean userIcaAllowsSpecContribution(PeopleDocumentData doc) {
        return doc.getDocumentID().equals(metadataConfig.ecaValidation().icaId());
    }

    /**
     * Checks if the current PeopleDocument is an Eclipse Contribution Agreement of the proper version that allows for spec project
     * contribution.
     * 
     * @param doc The current PeopleDocumentData to check
     * @return True if the document is an ECA with the correct version.
     */
    private boolean userEcaAllowsSpecContribution(PeopleDocumentData doc) {
        return doc.getDocumentID().equals(metadataConfig.ecaValidation().ecaId())
                && metadataConfig.ecaValidation().ecaVersion().contains(doc.getVersion());
    }

    /**
     * Builds a RelationTypeData object from the given relation type. Returns the passed in type with an empty description if it can't be
     * matched against any of the RelationType enum
     * 
     * @param type The current relation type.
     * @return A RelationTypeData object.
     */
    private RelationTypeData buildRelationType(String type) {
        RelationType relation = RelationType.getType(type);
        if (relation == null) {
            return RelationTypeData.builder().setType(type).setDescription("").build();
        }

        return RelationTypeData.builder().setType(relation.getName()).setDescription(relation.description).build();
    }
}
