/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.helpers;

import java.time.Instant;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests.AccountRequestsCompositeID;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * A helper class used build various entities and process data related to user delete requests.
 */
@ApplicationScoped
public final class UserDeleteHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDeleteHelper.class);

    @Inject
    TransformationHelper transformHelper;

    @Inject
    RequestWrapper wrap;

    /**
     * Builds a MultivaluedMap containing the desired ID param.
     * 
     * @param id The given id.
     * @return A MultivaluedMap containing the parsed int as a param.
     */
    public MultivaluedMap<String, String> buildIdParams(int id) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(id));
        return params;
    }

    /**
     * Builds an AccountRequests entity used to flag that a deletion request has been made.
     * 
     * @param wrap The current request wrapper
     * @param email The given user email
     * @param name The given username.
     * @return A populated AccountRequests entity.
     */
    public AccountRequests buildDeletionAccountRequest(String email, String username) {
        AccountRequests request = new AccountRequests();
        AccountRequestsCompositeID compID = new AccountRequestsCompositeID();
        compID.setEmail(email);
        compID.setReqWhen(Date.from(Instant.now()));
        request.setCompositeID(compID);
        request.setNewEmail("");
        request.setFname("DELETEACCOUNT");
        request.setLname("DELETEACCOUNT");
        request.setPassword(transformHelper.encryptAndEncodeValue(email));
        request.setIp(getBestMatchingIP());
        request.setToken(username);
        request.setPersonId("");
        return request;
    }

    /**
     * Retrieves the best IP for the current request, using the x-real-ip and x-forwarded-for headers to pull the information from the
     * current request.
     * 
     * @param wrap The RequestWrapper to pull the headers from
     * @return the best matching IP for current request, or localhost if none can be found
     */
    public String getBestMatchingIP() {
        LOGGER.debug("Looking up best IP address");

        // use real IP, followed by forwarded for to get the requests IP
        String initialIp = wrap.getHeader("X-Real-Ip");
        if (StringUtils.isBlank(initialIp)) {
            String[] forwardedIps = StringUtils.split(wrap.getHeader("X-Forwarded-For"), ",\\s*");
            if (forwardedIps != null && forwardedIps.length > 1) {
                initialIp = forwardedIps[1];
            }
        }
        // default to localhost if we can't find request information
        if (StringUtils.isBlank(initialIp)) {
            initialIp = "0.0.0.0";
        }
        return initialIp;
    }
}
