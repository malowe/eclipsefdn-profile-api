/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.efservices.api.models.EfUser.Email;
import org.eclipsefoundation.efservices.services.DrupalTokenService;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.full.FullPeopleProjectData;
import org.eclipsefoundation.profile.api.DrupalAccountsAPI;
import org.eclipsefoundation.profile.api.GerritAPI;
import org.eclipsefoundation.profile.api.OrganizationAPI;
import org.eclipsefoundation.profile.api.PeopleAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.api.models.GerritChangeData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;

/**
 * Helper class used to fetch from various sources for profile data
 */
@ApplicationScoped
public class ProfileHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileHelper.class);

    private static final Pattern ALTERNATE_EMAIL_PATTERN = Pattern
            .compile("alt-email\\s*:\\s*([\\w\\-\\.]+@[\\w\\-]+(\\.[\\w\\-]{2,4}){1,})");
    private static final int GERRIT_RESPONSE_START_INDEX = 4;
    private static final String ERROR_MSG = "Issue connecting to Accounts";

    @RestClient
    PeopleAPI peopleAPI;
    @RestClient
    OrganizationAPI orgAPI;

    @RestClient
    GerritAPI gerritAPI;
    @RestClient
    DrupalAccountsAPI accountsAPI;

    @Inject
    DrupalTokenService tokenService;

    @Inject
    ObjectMapper objectMapper;

    /**
     * Queries the accounts-api for profile data for the given user via username.
     * 
     * @param username The given username
     * @return An Optional containing the AccountsProfileData entity if it exists, or empty.
     */
    public Optional<AccountsProfileData> getAccountsProfileByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Accounts data for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return Optional.of(accountsAPI.getAccountsProfileByUsername(username, getBearerToken()));
        } catch (WebApplicationException e) {
            int status = e.getResponse().getStatus();
            if (status == Status.INTERNAL_SERVER_ERROR.getStatusCode()) {
                throw new ApplicationException(ERROR_MSG);
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("ACCOUNTS-API -  No account results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("ACCOUNTS-API - No account results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), status);
            }
            return Optional.empty();
        }
    }

    /**
     * Queries the accounts-api for profile data for the given user via drupal uid.
     * 
     * @param uid The given uid
     * @return An Optional containing the AccountsProfileData entity if it exists, or empty.
     */
    public Optional<AccountsProfileData> getAccountsProfileByUid(String uid) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Accounts profile data for user with uid: {}", uid);
        }
        try {
            return Optional.of(accountsAPI.getAccountsProfileByUid(uid, getBearerToken()).get(0));
        } catch (WebApplicationException e) {
            int status = e.getResponse().getStatus();
            if (status == Status.INTERNAL_SERVER_ERROR.getStatusCode()) {
                throw new ApplicationException(ERROR_MSG);
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("ACCOUNTS-API -  No account results for user: {}", TransformationHelper.formatLog(uid), e);
            } else {
                LOGGER
                        .warn("ACCOUNTS-API - No account results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(uid), status);
            }
            return Optional.empty();
        }
    }

    /**
     * Queries Fdndb-api for any active FullPeopleProject data for the given user.
     * 
     * @param username The given username
     * @return A List of FullPeopleProjectData or empty
     */
    public List<FullPeopleProjectData> getFullPeopleProjectsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleProjects for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return peopleAPI.getFullPeopleProjects(username, true).stream().collect(Collectors.toList());
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleProject results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleProject for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any OrganizationContactData data for the given user.
     * 
     * @param username The given username
     * @return A List of OrganizationContactData or empty
     */
    public List<OrganizationContactData> getOrgContactByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching OrganizationContactData for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return orgAPI.getOrgContacts(username);
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No OrganizationContact results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No OrganizationContact results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any FullOrganizationContactData data for the given user.
     * 
     * @param username The given username
     * @return A List of FullOrganizationContactData or empty
     */
    public List<FullOrganizationContactData> getFullOrgContactByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching FullOrganizationContactData for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return orgAPI.getFullOrgContacts(username);
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No OrganizationContact results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No OrganizationContact results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any non-expired document records signed by the given user.
     * 
     * @param username The given username
     * @return A List of PeopleDocumentData or empty
     */
    public List<PeopleDocumentData> getPeopleDocumentsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleDocuments for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return peopleAPI.getDocuments(username, true).stream().collect(Collectors.toList());
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleDocument results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleDocument results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any non-expired document records signed by the given org.
     * 
     * @param orgId The given orgId
     * @return A List of OrganizationDocumentData or empty
     */
    public List<OrganizationDocumentData> getDocumentsByOrg(String orgId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching OrganizationDocuments for org: {}", TransformationHelper.formatLog(orgId));
        }
        try {
            return orgAPI.getOrganizationDocuments(orgId, true).stream().collect(Collectors.toList());
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No OrganizationDocument results for org: {}", TransformationHelper.formatLog(orgId), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No OrganizationDocument results for org: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(orgId), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any PeopleEmailsData data for the given user.
     * 
     * @param username The given username
     * @return A List of PeopleEmailsData or empty
     */
    public List<Email> getEmailsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleEmailsData for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return peopleAPI
                    .getEmails(username)
                    .stream()
                    .map(e -> Email.builder().setId(e.getEmailID().toString()).setMail(e.getEmail()).build())
                    .sorted((e1, e2) -> e1.getId().compareTo(e2.getId()))
                    .toList();
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleEmailsData results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleEmailsData results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for a People record for the given user. And extracts the alternate emails from the comments field.
     * 
     * @param username The given username
     * @return A List of alternate email strings or empty
     */
    public List<String> getAlternateEmails(String username, Optional<PeopleData> loadedPerson) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching People entity: {}", TransformationHelper.formatLog(username));
        }
        try {
            // Get comments field from People record using pre-loaded user if possible, alt emails are stored there
            String comments = loadedPerson.isPresent() ? loadedPerson.get().getComments() : peopleAPI.getPerson(username).getComments();
            if (StringUtils.isBlank(comments)) {
                return Collections.emptyList();
            }

            List<String> alternateEmails = new ArrayList<>();

            // Loop through to find any potential matches
            Matcher matcher = ALTERNATE_EMAIL_PATTERN.matcher(comments);
            while (matcher.find()) {
                // Add first capturing group, which is the email
                alternateEmails.add(matcher.group(1));
            }

            return alternateEmails;
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No People results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No People results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries the Gerrit API for any changes made by a given user ignoring the first 'x' changes, where x is the current number of tracked
     * changes.
     * 
     * @param email The given user's email
     * @param start The starting cursor position
     * @return A List of GerritChangeData or empty
     */
    public List<GerritChangeData> getGerritChanges(String username, int start) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching gerrit changes for user: {}", username);
        }

        try {
            return getAllGerritChanges(username, start);
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GERRIT-API - No results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("GERRIT-API - No results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Fetches all Gerrit changes starting at previous count. Checks for the '_more_changes' field in the last record. If this field is set,
     * it will fetch the following page of results. It will fetch until there are no new changes.
     * 
     * @param query The search query
     * @param start The starting cursor position for fetching results.
     * @return A list of all the newest changes since the last query.
     */
    private List<GerritChangeData> getAllGerritChanges(String username, int start) {

        String query = "reviewer:" + username + " status:merged";

        List<GerritChangeData> out = new ArrayList<>();

        // Loop through all results until we hit the end flag
        while (true) {
            List<GerritChangeData> results = convertGerritJson(gerritAPI.getGerritChanges(query, start));
            out.addAll(results);

            // If '_more_changes' field exists in last record and is true, there is more
            if (results.isEmpty() || results.get(results.size() - 1).getMoreChanges() == null
                    || Boolean.FALSE.equals(results.get(results.size() - 1).getMoreChanges())) {
                break;
            }

            // increase the start point before getting next set
            start += results.size();
        }

        return out;
    }

    /**
     * Gerrit adds a special set of characters at the start of their response body. To be able to process the json properly, it must first
     * be removed. ex: )]}' [ ... valid JSON ... ]
     * 
     * @param body The Gerrit json body
     * @return A converted list of GerritchangeData entities if they exist
     */
    private List<GerritChangeData> convertGerritJson(String body) {
        try {
            // Parse the json body after the first 4 characters
            return objectMapper.readerForListOf(GerritChangeData.class).readValue(body.substring(GERRIT_RESPONSE_START_INDEX));
        } catch (Exception e) {
            LOGGER.error("Error parsing json Gerrit data", e);
            return Collections.emptyList();
        }
    }

    /**
     * Builds a bearer token using the token generated by the Oauth server. Will be removed once the accounts-api contains a way to get
     * account data via uid without auithentication.
     * 
     * @return A fully formed Bearer token
     */
    private String getBearerToken() {
        return "Bearer " + tokenService.getToken();
    }
}
