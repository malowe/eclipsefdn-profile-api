/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.profile.models.RequestStatusUpdate;
import org.eclipsefoundation.profile.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.profile.test.helpers.TestNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;

import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
class UserDeleteResourceTest {
    private static final String USER_DELETE_BASE_URL = "account/user_delete_request";
    private static final String USER_DELETE_BY_ID_URL = USER_DELETE_BASE_URL + "/{id}";

    private static final EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_DELETE_BASE_URL, new String[] {},
                    SchemaNamespaceHelper.USER_DELETE_REQUESTS_SCHEMA_PATH)
            .setStatusCode(200)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    private static final EndpointTestCase GET_SINGLE_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_DELETE_BY_ID_URL, new String[] { TestNamespaceHelper.VALID_DELETE_REQUEST_ID },
                    SchemaNamespaceHelper.USER_DELETE_REQUEST_SCHEMA_PATH)
            .setStatusCode(200)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    private static final EndpointTestCase GET_SINGLE_404 = TestCaseHelper
            .prepareTestCase(USER_DELETE_BY_ID_URL, new String[] { TestNamespaceHelper.INVALID_DELETE_REQUEST_ID },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setResponseContentType(ContentType.JSON)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    private static final EndpointTestCase GET_SINGLE_INVALID_AUTH = TestCaseHelper
            .prepareTestCase(USER_DELETE_BY_ID_URL, new String[] { TestNamespaceHelper.VALID_DELETE_REQUEST_ID },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(403)
            .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
            .build();

    private static final EndpointTestCase NO_CONTENT_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_DELETE_BY_ID_URL, new String[] { TestNamespaceHelper.VALID_DELETE_REQUEST_ID }, null)
            .setStatusCode(204)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    private static final EndpointTestCase PUT_SINGLE_400 = TestCaseHelper
            .prepareTestCase(USER_DELETE_BY_ID_URL, new String[] { TestNamespaceHelper.VALID_DELETE_REQUEST_ID }, null)
            .setStatusCode(400)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    @Inject
    ObjectMapper objectMapper;

    /*
     * GET ALL
     */
    @Test
    void testGetAllRequests_success() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    void testGetAllRequests_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetAllRequests_success_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetAllRequests_failure_invalidAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_DELETE_BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
                        .setStatusCode(403)
                        .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
                        .build())
                .run();
    }

    /*
     * GET SINGLE
     */
    @Test
    void testGetSingleRequests_success() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).run();
    }

    @Test
    void testGetSingleRequests_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetSingleRequests_success_validateSchema() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetSingleRequests_failure_notFound() {
        EndpointTestBuilder.from(GET_SINGLE_404).run();
    }

    @Test
    void testGetSingleRequests_failure_notFound_validateResponseFormat() {
        EndpointTestBuilder.from(GET_SINGLE_404).andCheckFormat().run();
    }

    @Test
    void testGetSingleRequests_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(GET_SINGLE_404).andCheckSchema().run();
    }

    @Test
    void testGetSingleRequests_failure_invalidAuth() {
        EndpointTestBuilder.from(GET_SINGLE_INVALID_AUTH).run();
    }

    /*
     * PUT UPDATE
     */
    @Test
    void testPutUpdate_success() {
        EndpointTestBuilder.from(NO_CONTENT_SUCCESS).doPut(buildUpdateBody(2)).run();
    }

    @Test
    void testPutUpdate_failure_badRequest_noBody() {
        EndpointTestBuilder.from(PUT_SINGLE_400).doPut("").run();
    }

    @Test
    void testPutUpdate_failure_badRequest_invalidStatus() {
        EndpointTestBuilder.from(PUT_SINGLE_400).doPut(buildUpdateBody(null)).run();
    }

    @Test
    void testPutUpdate_failure_notFound() {
        EndpointTestBuilder.from(GET_SINGLE_404).doPut(buildUpdateBody(2)).run();
    }

    @Test
    void testPutUpdate_failure_notFound_validateResponseformat() {
        EndpointTestBuilder.from(GET_SINGLE_404).doPut(buildUpdateBody(2)).andCheckFormat().run();
    }

    @Test
    void testPutUpdate_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(GET_SINGLE_404).doPut(buildUpdateBody(2)).andCheckSchema().run();
    }

    @Test
    void testPutUpdate_failure_invalidAuth() {
        EndpointTestBuilder.from(GET_SINGLE_INVALID_AUTH).doPut(buildUpdateBody(1)).run();
    }

    /*
     * DELETE
     */
    @Test
    void testDelete_success() {
        EndpointTestBuilder.from(NO_CONTENT_SUCCESS).doDelete(null).run();
    }

    @Test
    void testDelete_failure_notFound() {
        EndpointTestBuilder.from(GET_SINGLE_404).doDelete("").run();
    }

    @Test
    void testDelete_failure_notFound_validateResponseformat() {
        EndpointTestBuilder.from(GET_SINGLE_404).doDelete("").andCheckFormat().run();
    }

    @Test
    void testDelete_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(GET_SINGLE_404).doDelete("").andCheckSchema().run();
    }

    @Test
    void testDelete_failure_invalidAuth() {
        EndpointTestBuilder.from(GET_SINGLE_INVALID_AUTH).doDelete("").run();
    }

    private String buildUpdateBody(Integer newStatus) {
        try {
            return objectMapper
                    .writeValueAsString(RequestStatusUpdate.builder().setStatus(Optional.of(newStatus)).build());
        } catch (Exception e) {
            return "";
        }
    }
}
