/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Map;

import org.eclipsefoundation.profile.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.profile.test.helpers.TestNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class AccountResourceTest {
    public static final String BASE_URL = "account/profile";
    public static final String USER_SEARCH_EMAIL_URL = BASE_URL + "?mail={param}";
    public static final String USER_SEARCH_UID_URL = BASE_URL + "?uid={param}";
    public static final String USER_SEARCH_NAME_URL = BASE_URL + "?name={param}";
    public static final String PROFILE_BY_USER_URL = BASE_URL + "/{username}";
    public static final String ECA_URL = PROFILE_BY_USER_URL + "/eca";
    public static final String MAILING_LIST_URL = PROFILE_BY_USER_URL + "/mailing-list";
    public static final String PROJECTS_URL = PROFILE_BY_USER_URL + "/projects";
    public static final String FORUM_URL = PROFILE_BY_USER_URL + "/forum";
    public static final String GERRIT_URL = PROFILE_BY_USER_URL + "/gerrit";
    public static final String USER_DELETE_URL = "account/profile/{username}/user_delete_request";

    /*
     * BASE + PARAMS
     */
    public static final EndpointTestCase GET_CURRENT_USER_SUCCESS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.EF_USERS_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_USER_AUTH_HEADER)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_UID_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_SEARCH_UID_URL, new String[] { TestNamespaceHelper.VALID_UID },
                    SchemaNamespaceHelper.EF_USERS_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_UID_NOT_FOUND = TestCaseHelper
            .prepareTestCase(USER_SEARCH_UID_URL, new String[] { TestNamespaceHelper.INVALID_UID }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .setStatusCode(404)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_MAIL_LDAP_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_SEARCH_EMAIL_URL, new String[] { TestNamespaceHelper.VALID_MAIL },
                    SchemaNamespaceHelper.EF_USERS_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_MAIL_FDNDB_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_SEARCH_EMAIL_URL, new String[] { "barshallblathers@email.com" },
                    SchemaNamespaceHelper.EF_USERS_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_MAIL_NOT_FOUND = TestCaseHelper
            .prepareTestCase(USER_SEARCH_EMAIL_URL, new String[] { TestNamespaceHelper.INVALID_EMAIL },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .setStatusCode(404)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_NAME_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_SEARCH_NAME_URL, new String[] { TestNamespaceHelper.VALID_USERNAME },
                    SchemaNamespaceHelper.EF_USERS_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();
    public static final EndpointTestCase SEARCH_USER_BY_NAME_NOT_FOUND = TestCaseHelper
            .prepareTestCase(USER_SEARCH_NAME_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .setStatusCode(404)
            .build();

    /*
     * BY USERNAME
     */
    public static final EndpointTestCase GET_BY_USER_CASE_SUCCESS_AUTH = TestCaseHelper
            .prepareTestCase(PROFILE_BY_USER_URL, new String[] { TestNamespaceHelper.VALID_USERNAME },
                    SchemaNamespaceHelper.EF_USER_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .setBodyValidationParams(Map.of("mail", TestNamespaceHelper.VALID_MAIL))
            .build();

    public static final EndpointTestCase GET_BY_USER_SUCCESS_BAD_AUTH = TestCaseHelper
            .prepareTestCase(PROFILE_BY_USER_URL, new String[] { TestNamespaceHelper.VALID_USERNAME },
                    SchemaNamespaceHelper.EF_USER_SCHEMA_PATH)
            .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
            .setBodyValidationParams(Map.of("mail", ""))
            .build();

    public static final EndpointTestCase GET_BY_USER_CASE_NOT_FOUND = TestCaseHelper
            .prepareTestCase(PROFILE_BY_USER_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    /*
     * ECA
     */
    public static final EndpointTestCase GET_ECA_SUCCESS = TestCaseHelper
            .buildSuccessCase(ECA_URL, new String[] { TestNamespaceHelper.VALID_USERNAME }, SchemaNamespaceHelper.ECA_SCHEMA_PATH);

    public static final EndpointTestCase GET_ECA_INVALID_USER = TestCaseHelper
            .buildSuccessCase(ECA_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME }, SchemaNamespaceHelper.ECA_SCHEMA_PATH);

    /*
     * MAILING-LIST
     */
    public static final EndpointTestCase GET_MAILING_LIST_SUCCESS = TestCaseHelper
            .buildSuccessCase(MAILING_LIST_URL, new String[] { TestNamespaceHelper.VALID_USERNAME },
                    SchemaNamespaceHelper.SUBSCRIPTIONS_SCHEMA_PATH);

    public static final EndpointTestCase GET_MAILING_LIST_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(MAILING_LIST_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * PROJECTS
     */
    public static final EndpointTestCase GET_PROJECTS_SUCCESS = TestCaseHelper
            .buildSuccessCase(PROJECTS_URL, new String[] { TestNamespaceHelper.VALID_USERNAME },
                    SchemaNamespaceHelper.PEOPLE_PROJECT_MAP_PATH);

    public static final EndpointTestCase GET_PROJECTS_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(PROJECTS_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GERRIT
     */
    public static final EndpointTestCase GET_GERRIT_SUCCESS = TestCaseHelper
            .buildSuccessCase(GERRIT_URL, new String[] { TestNamespaceHelper.VALID_USERNAME },
                    SchemaNamespaceHelper.GERRIT_RESPONSE_SCHEMA_PATH);

    public static final EndpointTestCase GET_GERRIT_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(GERRIT_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * USER DELETE REQUEST
     */
    public static final EndpointTestCase CREATE_DELETE_REQUEST_NOT_FOUND = TestCaseHelper
            .prepareTestCase(USER_DELETE_URL, new String[] { TestNamespaceHelper.INVALID_USERNAME },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
            .build();

    /*
     * GET /account/profile
     */
    @Test
    void testGetCurrent_success() {
        EndpointTestBuilder.from(GET_CURRENT_USER_SUCCESS).run();
    }

    @Test
    void testGetUserCurrent_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_CURRENT_USER_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetUserCurrent_success_validateSchema() {
        EndpointTestBuilder.from(GET_CURRENT_USER_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetUserCurrent_failure_noAuth() {
        // No auth header
        EndpointTestBuilder.from(TestCaseHelper.buildForbiddenCase(BASE_URL, new String[] {}, null)).run();
    }

    @Test
    void testGetUserCurrent_failure_invalidAuth() {
        // Valid but anonymous auth token
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(BASE_URL, new String[] {}, null)
                        .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
                        .setStatusCode(403)
                        .build())
                .run();
    }

    @Test
    void testGetSearchUid_success() {
        EndpointTestBuilder.from(SEARCH_USER_BY_UID_SUCCESS).run();
    }

    @Test
    void testGetSearchUid_success_validateResponseFormat() {
        EndpointTestBuilder.from(SEARCH_USER_BY_UID_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetSearchUid_success_validateSchema() {
        EndpointTestBuilder.from(SEARCH_USER_BY_UID_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetSearchUid_failure_noAuth() {
        // No auth header
        EndpointTestBuilder.from(TestCaseHelper.buildForbiddenCase(USER_SEARCH_UID_URL, new String[] { "1" }, null)).run();
    }

    @Test
    void testGetSearchUid_failure_invalidAuth() {
        // Valid but anonymous auth token
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_SEARCH_UID_URL, new String[] { "1" }, null)
                        .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
                        .setStatusCode(403)
                        .build())
                .run();
    }

    @Test
    void testGetSearchUid_failure_notFound() {
        EndpointTestBuilder.from(SEARCH_USER_BY_UID_NOT_FOUND).run();
    }

    @Test
    void testGetSearchUid_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(SEARCH_USER_BY_UID_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void testGetSearchEmail_success_ldap() {
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_LDAP_SUCCESS).run();
    }

    @Test
    void testGetSearchEmail_success_ldap_validateResponseFormat() {
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_LDAP_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetSearchEmail_success_ldap_validateSchema() {
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_LDAP_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetSearchEmail_success_fdndb() {
        // This user is in fdndb
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_FDNDB_SUCCESS).run();
    }

    @Test
    void testGetSearchEmail_success_fdndb_validateResponseFormat() {
        // This user is in fdndb
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_FDNDB_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetSearchEmail_success_fdndb_validateSchema() {
        // This user is in fdndb
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_FDNDB_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetSearchEmail_failure_noAuth() {
        // No auth header
        EndpointTestBuilder
                .from(TestCaseHelper.buildForbiddenCase(USER_SEARCH_EMAIL_URL, new String[] { TestNamespaceHelper.VALID_MAIL }, null))
                .run();
    }

    @Test
    void testGetSearchEmail_failure_invalidAuth() {
        // Valid but anonymous auth token
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_SEARCH_EMAIL_URL, new String[] { TestNamespaceHelper.VALID_MAIL }, null)
                        .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
                        .setStatusCode(403)
                        .build())
                .run();
    }

    @Test
    void testGetSearchEmail_failure_notFound() {
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_NOT_FOUND).run();
    }

    @Test
    void testGetSearchEmail_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(SEARCH_USER_BY_MAIL_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void testGetSearchName_success() {
        EndpointTestBuilder.from(SEARCH_USER_BY_NAME_SUCCESS).run();
    }

    @Test
    void testGetSearchName_success_validateResponseFormat() {
        EndpointTestBuilder.from(SEARCH_USER_BY_NAME_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetSearchName_success_validateSchema() {
        EndpointTestBuilder.from(SEARCH_USER_BY_NAME_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetSearchName_failure_noAuth() {
        // No auth header
        EndpointTestBuilder
                .from(TestCaseHelper.buildForbiddenCase(USER_SEARCH_NAME_URL, new String[] { TestNamespaceHelper.VALID_USERNAME }, null))
                .run();
    }

    @Test
    void testGetSearchName_failure_invalidAuth() {
        // Valid but anonymous auth token
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_SEARCH_NAME_URL, new String[] { TestNamespaceHelper.VALID_USERNAME }, null)
                        .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
                        .setStatusCode(403)
                        .build())
                .run();
    }

    @Test
    void testGetSearchName_failure_notFound() {
        EndpointTestBuilder.from(SEARCH_USER_BY_NAME_NOT_FOUND).run();
    }

    @Test
    void testGetSearchName_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(SEARCH_USER_BY_NAME_NOT_FOUND).andCheckSchema().run();
    }

    /*
     * GET /account/profile/{username}
     */
    @Test
    void testGetUserByUsername_success() {
        EndpointTestBuilder.from(GET_BY_USER_CASE_SUCCESS_AUTH).run();
    }

    @Test
    void testGetUserByUsername_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_USER_CASE_SUCCESS_AUTH).andCheckFormat().run();
    }

    @Test
    void testGetUserByUsername_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_USER_CASE_SUCCESS_AUTH).andCheckSchema().run();
    }

    @Test
    void testGetUserByUsername_failure_notfound() {
        EndpointTestBuilder.from(GET_BY_USER_CASE_NOT_FOUND).run();
    }

    @Test
    void testGetUserByUsername_failure_notfound_validateSchema() {
        EndpointTestBuilder.from(GET_BY_USER_CASE_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void testGetUserByUsernameBadauth_success() {
        EndpointTestBuilder.from(GET_BY_USER_SUCCESS_BAD_AUTH).run();
    }

    @Test
    void testGetUserByUsernameBadAuth_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_USER_SUCCESS_BAD_AUTH).andCheckSchema().run();
    }

    @Test
    void testGetUserByUsernameBadAuth_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_USER_SUCCESS_BAD_AUTH).andCheckFormat().run();
    }

    /*
     * GET /account/profile/{username}/eca
     */
    @Test
    void testGetEca_success() {
        EndpointTestBuilder.from(GET_ECA_SUCCESS).run();
    }

    @Test
    void testGetEca_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ECA_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetEca_success_validateSchema() {
        EndpointTestBuilder.from(GET_ECA_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetEca_failure_notfound() {
        EndpointTestBuilder.from(GET_ECA_INVALID_USER).run();
    }

    @Test
    void testGetEca_failure_notfound_validateSchema() {
        EndpointTestBuilder.from(GET_ECA_INVALID_USER).andCheckSchema().run();
    }

    /*
     * GET /account/profile/{username}/mailing-list
     */
    @Test
    void testGetMailing_success() {
        EndpointTestBuilder.from(GET_MAILING_LIST_SUCCESS).run();
    }

    @Test
    void testGetMailing_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_MAILING_LIST_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetMailing_success_validateSchema() {
        EndpointTestBuilder.from(GET_MAILING_LIST_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetMailing_failure_notfound() {
        EndpointTestBuilder.from(GET_MAILING_LIST_NOT_FOUND).run();
    }

    @Test
    void testGetMailing_failure_notfound_validateSchema() {
        EndpointTestBuilder.from(GET_MAILING_LIST_NOT_FOUND).andCheckSchema().run();
    }

    /*
     * GET /account/profile/{username}/projects
     */
    @Test
    void testGetProjects_success() {
        EndpointTestBuilder.from(GET_PROJECTS_SUCCESS).run();
    }

    @Test
    void testGetProjects_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_PROJECTS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetProjects_success_validateSchema() {
        EndpointTestBuilder.from(GET_PROJECTS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetProjects_failure_notfound() {
        EndpointTestBuilder.from(GET_PROJECTS_NOT_FOUND).run();
    }

    @Test
    void testGetProjects_failure_notfound_validateSchema() {
        EndpointTestBuilder.from(GET_PROJECTS_NOT_FOUND).andCheckSchema().run();
    }

    /*
     * GET /account/profile/{username}/gerrit
     */
    @Test
    void testGetGerrit_success() {
        EndpointTestBuilder.from(GET_GERRIT_SUCCESS).run();
    }

    @Test
    void testGetGerrit_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_GERRIT_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetGerrit_success_validateSchema() {
        EndpointTestBuilder.from(GET_GERRIT_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetGerrit_failure_notfound() {
        EndpointTestBuilder.from(GET_GERRIT_NOT_FOUND).run();
    }

    @Test
    void testGetGerrit_failure_notfound_validateSchema() {
        EndpointTestBuilder.from(GET_GERRIT_NOT_FOUND).andCheckSchema().run();
    }

    /*
     * GET /account/profile/{username}/user_delete_request
     */
    @Test
    void testPostDeleteRequest_success() {

        EndpointTestBuilder.from(buildDeleteTestCaseForUser("firstlast")).doPost(null).run();

        // Test again to validate conflict response
        EndpointTestBuilder.from(buildConflictDeleteCaseForUser("firstlast")).doPost(null).run();
    }

    @Test
    void testPostDeleteRequest_success_validateResponseFormat() {

        // Use different person than above test because of 409 response
        EndpointTestBuilder.from(buildDeleteTestCaseForUser("fakeperson")).doPost(null).run();

        // Test again to validate conflict response
        EndpointTestBuilder.from(buildConflictDeleteCaseForUser("fakeperson")).doPost(null).run();
    }

    @Test
    void testPostDeleteRequest_success_validateSchema() {
        // Use different person than above test because of 409 response
        EndpointTestBuilder.from(buildDeleteTestCaseForUser("barshallblathers")).doPost(null).run();

        // Test again to validate conflict response
        EndpointTestBuilder.from(buildConflictDeleteCaseForUser("barshallblathers")).doPost(null).run();
    }

    @Test
    void testPostDeleteRequest_failure_notFound() {
        EndpointTestBuilder.from(CREATE_DELETE_REQUEST_NOT_FOUND).doPost(null).run();
    }

    @Test
    void testPostDeleteRequest_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(CREATE_DELETE_REQUEST_NOT_FOUND).doPost(null).andCheckSchema().run();
    }

    @Test
    void testPostDeleteRequest_failure_invalidAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_DELETE_URL, new String[] { "firstlast" },
                                SchemaNamespaceHelper.USER_DELETE_REQUESTS_SCHEMA_PATH)
                        .setHeaderParams(TestNamespaceHelper.INVALID_ANON_AUTH_HEADER)
                        .setStatusCode(403)
                        .build())
                .doPost(null)
                .run();
    }

    private EndpointTestCase buildDeleteTestCaseForUser(String user) {
        return TestCaseHelper
                .prepareTestCase(USER_DELETE_URL, new String[] { user }, SchemaNamespaceHelper.USER_DELETE_REQUESTS_SCHEMA_PATH)
                .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
                .setStatusCode(201)
                .build();
    }

    private EndpointTestCase buildConflictDeleteCaseForUser(String user) {
        return TestCaseHelper
                .prepareTestCase(USER_DELETE_URL, new String[] { user }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
                .setHeaderParams(TestNamespaceHelper.VALID_ANON_AUTH_HEADER)
                .setStatusCode(409)
                .build();
    }
}
