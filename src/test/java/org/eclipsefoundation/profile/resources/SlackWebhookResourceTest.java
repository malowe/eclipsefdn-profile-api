/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.profile.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.profile.test.helpers.TestNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

@QuarkusTest
class SlackWebhookResourceTest {
    private static final String BASE_URL = "account/webhook/slack/u";

    private static final String VALID_SLACK_TOKEN = "slackToken";
    private static final String INVALID_SLACK_TOKEN = "badToken";
    private static final String VALID_TEAM_DOMAIN = "eclipsefoundation";
    private static final String INVALID_TEAM_DOMAIN = "some_company";

    private static final String VALID_USERNAME_REQUEST = createRequestFormParams(TestNamespaceHelper.VALID_USERNAME, VALID_SLACK_TOKEN,
            VALID_TEAM_DOMAIN);

    private static final String VALID_EMAIL_REQUEST = createRequestFormParams(TestNamespaceHelper.VALID_MAIL, VALID_SLACK_TOKEN,
            VALID_TEAM_DOMAIN);

    private static final String INVALID_VALUE_REQUEST = createRequestFormParams("nope", VALID_SLACK_TOKEN, VALID_TEAM_DOMAIN);

    private static final String INVALID_DOMAIN_REQUEST = createRequestFormParams(TestNamespaceHelper.VALID_USERNAME, VALID_SLACK_TOKEN,
            INVALID_TEAM_DOMAIN);

    private static final String INVALID_TOKEN_REQUEST = createRequestFormParams(TestNamespaceHelper.VALID_USERNAME, INVALID_SLACK_TOKEN,
            INVALID_TEAM_DOMAIN);

    private static final EndpointTestCase SEARCH_BY_VALUE_SUCCESS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.SLACK_RESPONSE_SCHEMA_PATH)
            .setRequestContentType(ContentType.URLENC)
            .build();

    private static final EndpointTestCase SEARCH_FORBIDDEN = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, null)
            .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
            .setRequestContentType(ContentType.URLENC)
            .build();;

    @Inject
    ObjectMapper om;

    @Test
    void testFindProfileByUsername_success() {
        ValidatableResponse response = EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(VALID_USERNAME_REQUEST).run();
        int attachmentSize = response.extract().body().jsonPath().getList("attachments").size();
        Assertions.assertEquals(1, attachmentSize, "There should be 1 attachment containing the profile data");
    }

    @Test
    void testFindProfileByUsername_success_validateResponseFormat() {
        EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(VALID_USERNAME_REQUEST).andCheckFormat().run();
    }

    @Test
    void testFindProfileByUsername_success_validateSchema() {
        EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(VALID_USERNAME_REQUEST).andCheckSchema().run();
    }

    @Test
    void testFindProfileByEmail_success() {
        ValidatableResponse response = EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(VALID_EMAIL_REQUEST).run();
        int attachmentSize = response.extract().body().jsonPath().getList("attachments").size();
        Assertions.assertEquals(1, attachmentSize, "There should be 1 attachment containing the profile data");
    }

    @Test
    void testFindProfileByEmail_success_validateResponseFormat() {
        EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(VALID_EMAIL_REQUEST).andCheckFormat().run();
    }

    @Test
    void testFindProfileByEmail_success_validateSchema() {
        EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(VALID_EMAIL_REQUEST).andCheckSchema().run();
    }

    @Test
    void testFindProfile_success_notFound() {
        ValidatableResponse response = EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(INVALID_VALUE_REQUEST).run();
        int attachmentSize = response.extract().body().jsonPath().getList("attachments").size();
        Assertions.assertEquals(0, attachmentSize, "There should be 0 attachments when a profile is not found");
    }

    @Test
    void testFindProfile_success_notFound_validateResponseFormat() {
        EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(INVALID_VALUE_REQUEST).andCheckFormat().run();
    }

    @Test
    void testFindProfile_success_notFound_validateSchema() {
        EndpointTestBuilder.from(SEARCH_BY_VALUE_SUCCESS).doPost(INVALID_VALUE_REQUEST).andCheckSchema().run();
    }

    @Test
    void testFindProfile_failure_invalidTeamDomain() {
        EndpointTestBuilder.from(SEARCH_FORBIDDEN).doPost(INVALID_DOMAIN_REQUEST).run();
    }

    @Test
    void testFindProfile_failure_invalidToken() {
        EndpointTestBuilder.from(SEARCH_FORBIDDEN).doPost(INVALID_TOKEN_REQUEST).run();
    }

    private static String createRequestFormParams(String text, String token, String teamDomain) {
        StringBuilder builder = new StringBuilder();
        builder.append("text=");
        builder.append(text);
        builder.append("&token=");
        builder.append(token);
        builder.append("&team_domain=");
        builder.append(teamDomain);
        return builder.toString();
    }
}
