/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.helpers;

public class SchemaNamespaceHelper {
    public static final String BASE_SCHEMA_PATH = "schemas/";
    public static final String BASE_SCHEMA_PATH_SUFFIX = "-schema.json";

    public static final String EF_USER_SCHEMA_PATH = BASE_SCHEMA_PATH + "ef-user" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String EF_USERS_SCHEMA_PATH = BASE_SCHEMA_PATH + "ef-users" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMA_PATH + "error" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String ECA_SCHEMA_PATH = BASE_SCHEMA_PATH + "eca" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String PEOPLE_PROJECT_MAP_PATH = BASE_SCHEMA_PATH + "people-project-map" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String SUBSCRIPTIONS_SCHEMA_PATH = BASE_SCHEMA_PATH + "subscriptions" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String GERRIT_RESPONSE_SCHEMA_PATH = BASE_SCHEMA_PATH + "gerrit-response" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String USER_DELETE_REQUESTS_SCHEMA_PATH = BASE_SCHEMA_PATH + "user-delete-requests" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String USER_DELETE_REQUEST_SCHEMA_PATH = BASE_SCHEMA_PATH + "user-delete-request" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String SLACK_RESPONSE_SCHEMA_PATH = BASE_SCHEMA_PATH + "slack-response" + BASE_SCHEMA_PATH_SUFFIX;
}
