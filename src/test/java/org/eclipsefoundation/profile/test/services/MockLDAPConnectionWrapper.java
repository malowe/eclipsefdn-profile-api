/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.profile.namespace.LdapFieldNames;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.LDAPConnectionWrapper;
import org.eclipsefoundation.profile.test.helpers.TestNamespaceHelper;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;

import io.quarkus.test.Mock;

@Mock
@ApplicationScoped
public class MockLDAPConnectionWrapper implements LDAPConnectionWrapper {

    private List<SearchResultEntry> entries;

    public MockLDAPConnectionWrapper() {
        entries = new ArrayList<>();
        entries
                .addAll(Arrays
                        .asList(generateEntry(TestNamespaceHelper.VALID_USERNAME, TestNamespaceHelper.VALID_MAIL,
                                TestNamespaceHelper.VALID_HANDLE, TestNamespaceHelper.VALID_FIRST_NAME,
                                TestNamespaceHelper.VALID_LAST_NAME),
                                generateEntry("firstlast", "email@website.com", "firstlast", "first", "last"),
                                generateEntry("barshallblathers", "barshallblathers@email.com", "barshall", "barshall", "blathers"),
                                generateEntry("fakeperson", "email@unreal.com", "handle", "fake", "person")));
    }

    @Override
    public List<SearchResultEntry> connectAndSearch(SearchRequest request) {
        String searchField = request.getFilter().getAttributeName();
        String searchValue = request.getFilter().getAssertionValue();

        return entries
                .stream()
                .filter(e -> e.getAttribute(searchField).getValue().equalsIgnoreCase(searchValue))
                .collect(Collectors.toList());
    }

    /*
     * Generates a SearchResultEntry using the desired fields as values. The real LDAP entries will have many more attributes, but this
     * application is only concerned with these.
     */
    private SearchResultEntry generateEntry(String username, String mail, String handle, String fname, String lName) {
        return new SearchResultEntry("dn=sample",
                new Attribute[] {
                        new Attribute(ProfileAPIParameterNames.UID.getName(), username),
                        new Attribute(ProfileAPIParameterNames.MAIL.getName(), mail),
                        new Attribute(LdapFieldNames.EMPLOYEE_TYPE, "GITHUB:" + handle),
                        new Attribute(LdapFieldNames.SN, fname),
                        new Attribute(LdapFieldNames.GIVEN_NAME, lName), 
                        new Attribute(LdapFieldNames.CN, fname + lName) },
                new Control[] {});
    }

}
