/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.profile.api.SysAPI;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockSysAPI implements SysAPI {

    private List<SysModLogData> logs;

    public MockSysAPI() {
        this.logs = new ArrayList<>();
    }

    @Override
    public List<SysModLogData> insertSysModLog(SysModLogData src) {
        this.logs.add(src);
        return Arrays.asList(src);
    }
}
