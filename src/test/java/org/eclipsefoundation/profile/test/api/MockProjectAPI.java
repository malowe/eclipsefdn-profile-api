/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.GitlabProject;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.Project.GithubProject;
import org.eclipsefoundation.efservices.api.models.Project.ProjectParticipant;
import org.eclipsefoundation.efservices.api.models.Project.Repo;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockProjectAPI implements ProjectsAPI {

    private List<Project> specProjects;

    public MockProjectAPI() {
        this.specProjects = new ArrayList<>();

        // sample repos
        Repo r1 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/sample").build();
        Repo r2 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/test").build();
        Repo r3 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/tck-proto").build();
        Repo r4 = Repo.builder().setUrl("/gitroot/sample/gerrit.project.git").build();

        // sample users
        ProjectParticipant u1 = ProjectParticipant.builder().setUrl("").setUsername("da_wizz").setFullName("da_wizz")
                .build();
        ProjectParticipant u2 = ProjectParticipant.builder().setUrl("").setUsername("grunter").setFullName("grunter")
                .build();

        this.specProjects.add(Project.builder()
                .setName("Some project")
                .setProjectId("some.project")
                .setSpecProjectWorkingGroup(Map.of("id", "proj1", "name", "proj1"))
                .setGithubRepos(Arrays.asList(r1, r2))
                .setGerritRepos(Arrays.asList(r4))
                .setCommitters(Arrays.asList(u1, u2))
                .setProjectLeads(Collections.emptyList())
                .setGitlab(GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList()).setProjectGroup("")
                        .build())
                .setShortProjectId("sample.proj")
                .setSummary("summary")
                .setUrl("project.url.com")
                .setWebsiteUrl("someproject.com")
                .setWebsiteRepo(Collections.emptyList())
                .setLogo("logoUrl.com")
                .setTags(Collections.emptyList())
                .setGithub(GithubProject.builder()
                        .setOrg("org name")
                        .setIgnoredRepos(Collections.emptyList()).build())
                .setContributors(Collections.emptyList())
                .setWorkingGroups(Collections.emptyList())
                .setIndustryCollaborations(Collections.emptyList())
                .setReleases(Collections.emptyList())
                .setTopLevelProject("eclipse")
                .build());

        this.specProjects.add(Project.builder()
                .setName("Other project")
                .setProjectId("other.project")
                .setSpecProjectWorkingGroup(Map.of("id", "proj1", "name", "proj1"))
                .setGithubRepos(Arrays.asList(r3))
                .setGerritRepos(Collections.emptyList())
                .setGitlab(GitlabProject
                        .builder()
                        .setIgnoredSubGroups(Arrays.asList("eclipse/dash/mirror"))
                        .setProjectGroup("eclipse/dash")
                        .build())
                .setCommitters(Arrays.asList(u1, u2))
                .setProjectLeads(Collections.emptyList())
                .setShortProjectId("spec.proj")
                .setSummary("summary")
                .setUrl("project.url.com")
                .setWebsiteUrl("someproject.com")
                .setWebsiteRepo(Collections.emptyList())
                .setLogo("logoUrl.com")
                .setTags(Collections.emptyList())
                .setGithub(GithubProject.builder()
                        .setOrg("org name")
                        .setIgnoredRepos(Collections.emptyList()).build())
                .setContributors(Collections.emptyList())
                .setWorkingGroups(Collections.emptyList())
                .setIndustryCollaborations(Collections.emptyList())
                .setReleases(Collections.emptyList())
                .setTopLevelProject("eclipse")
                .build());
    }

    @Override
    public Response getProjects(BaseAPIParameters params, int isSpecProject) {
        return Response.ok(isSpecProject == 1 ? specProjects : Collections.emptyList()).build();
    }

    @Override
    public Response getInterestGroups(BaseAPIParameters arg0) {
        return Response.ok(Collections.emptyList()).build();
    }
}
