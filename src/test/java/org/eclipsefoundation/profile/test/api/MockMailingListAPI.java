/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.profile.api.MailingListAPI;
import org.eclipsefoundation.profile.api.models.MailingListData;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockMailingListAPI implements MailingListAPI {

    private Map<String, List<MailingListData>> internal;

    public MockMailingListAPI() {
        this.internal = new HashMap<>();
        this.internal
                .put("username",
                        Arrays
                                .asList(MailingListData.builder().setListName("list-one").setListDescription("list-desc").build(),
                                        MailingListData.builder().setListName("list-two").setListDescription("list-desc").build()));
    }

    @Override
    public List<MailingListData> getMailingListsByUsername(String username, Boolean isSubscribable) {
        List<MailingListData> results = internal.getOrDefault(username, Collections.emptyList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }
}
