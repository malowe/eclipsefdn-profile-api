/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.Arrays;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.profile.api.GerritAPI;
import org.eclipsefoundation.profile.api.models.GerritChangeData;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockGerritAPI implements GerritAPI {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockGerritAPI.class);

    private static final String SPECIAL_CHARS = ")]}'";

    private MultivaluedMap<String, GerritChangeData> changes;

    @Inject
    ObjectMapper objectMapper;

    public MockGerritAPI() {
        this.changes = new MultivaluedMapImpl<>();
        this.changes
                .put("firstlast",
                        Arrays
                                .asList(buildGerritChange("1"), buildGerritChange("2"), buildGerritChange("3"), buildGerritChange("4"),
                                        buildGerritChange("5")));

        this.changes
                .put("username",
                        Arrays
                                .asList(buildGerritChange("1"), buildGerritChange("2"), buildGerritChange("3"), buildGerritChange("4"),
                                        buildGerritChange("5")));
    }

    @Override
    public String getGerritChanges(String query, int start) {
        try {
            String username = query.substring(query.indexOf(":") + 1, query.indexOf(" "));

            List<GerritChangeData> results = changes.get(username);

            String json = objectMapper.writeValueAsString(results);

            return SPECIAL_CHARS + json;

        } catch (Exception e) {
            LOGGER.error("Error serializing gerrit changes", e);
            return SPECIAL_CHARS + "[]";
        }
    }

    private GerritChangeData buildGerritChange(String changeId) {
        return GerritChangeData
                .builder()
                .setId("www.eclipse.org?change_id=" + changeId)
                .setChangeId(changeId)
                .setProject("www.eclipse.org/org")
                .setStatus("MERGED")
                .build();
    }
}
