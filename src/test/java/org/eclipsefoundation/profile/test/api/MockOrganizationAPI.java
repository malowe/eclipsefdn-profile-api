/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.foundationdb.client.model.full.FullOrganizationContactData;
import org.eclipsefoundation.profile.api.OrganizationAPI;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockOrganizationAPI implements OrganizationAPI {

    private List<OrganizationDocumentData> documents;
    private List<FullOrganizationContactData> fullOrgContacts;
    private List<OrganizationContactData> orgContacts;

    public MockOrganizationAPI() {
        this.documents = new ArrayList<>();
        this.documents
                .addAll(Arrays
                        .asList(OrganizationDocumentData.builder().setOrganizationID(42).setDocumentID("mccaId").setVersion(1).build(),
                                OrganizationDocumentData
                                        .builder()
                                        .setOrganizationID(42)
                                        .setDocumentID("someOtherDoc")
                                        .setVersion(1)
                                        .build()));

        this.orgContacts = new ArrayList<>();
        this.orgContacts
                .addAll(Arrays
                        .asList(OrganizationContactData
                                .builder()
                                .setOrganizationID(42)
                                .setPersonID("username")
                                .setRelation("EMPLY")
                                .setDocumentID(null)
                                .setComments("PRIMARY")
                                .setTitle("Head honcho")
                                .build()));

        this.fullOrgContacts = new ArrayList<>();
        this.fullOrgContacts
                .addAll(Arrays
                        .asList(FullOrganizationContactData
                                .builder()
                                .setOrganization(OrganizationData
                                        .builder()
                                        .setOrganizationID(42)
                                        .setName1("LovelyOrg")
                                        .setName2("")
                                        .setPhone("911")
                                        .setFax("911-2")
                                        .setComments("Lovely org")
                                        .setTimestamp(new Date())
                                        .build())
                                .setPerson(PeopleData
                                        .builder()
                                        .setPersonID("username")
                                        .setFname("user")
                                        .setLname("name")
                                        .setType("XX")
                                        .setMember(false)
                                        .setEmail("email@fake.com")
                                        .setUnixAcctCreated(false)
                                        .setIssuesPending(false)
                                        .build())
                                .setDocumentID(null)
                                .setSysRelation(SysRelationData
                                        .builder()
                                        .setRelation("EMPLY")
                                        .setDescription("Employee")
                                        .setActive(true)
                                        .setType("CO")
                                        .build())
                                .setComments("PRIMARY")
                                .setTitle("Head honcho")
                                .build()));
    }

    @Override
    public List<OrganizationDocumentData> getOrganizationDocuments(String orgId, boolean isNotExpired) {
        List<OrganizationDocumentData> results = documents
                .stream()
                .filter(d -> d.getOrganizationID() == Integer.valueOf(orgId))
                .collect(Collectors.toList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<FullOrganizationContactData> getFullOrgContacts(String username) {
        List<FullOrganizationContactData> results = fullOrgContacts
                .stream()
                .filter(c -> c.getPerson().getPersonID().equalsIgnoreCase(username))
                .collect(Collectors.toList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<OrganizationContactData> getOrgContacts(String username) {
        List<OrganizationContactData> results = orgContacts
                .stream()
                .filter(c -> c.getPersonID().equalsIgnoreCase(username))
                .collect(Collectors.toList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }
}
