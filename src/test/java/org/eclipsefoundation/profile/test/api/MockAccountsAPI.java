/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.models.EfUser.Country;
import org.eclipsefoundation.profile.api.DrupalAccountsAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockAccountsAPI implements DrupalAccountsAPI {

    private List<AccountsProfileData> users;

    public MockAccountsAPI() {
        this.users = new ArrayList<>();
        this.users
                .addAll(Arrays
                        .asList(AccountsProfileData
                                .builder()
                                .setUid("666")
                                .setMail("test@mail.ca")
                                .setName("username")
                                .setFirstName("user")
                                .setLastName("name")
                                .setFullName("user name")
                                .setTwitterHandle("handle")
                                .setOrg("Eclipse")
                                .setJobTitle("boss")
                                .setWebsite("google.com")
                                .setCountry(Country.builder().setCode("CA").setName("Canada").build())
                                .setBio("Likes do do things")
                                .setInterests(Arrays.asList("item1", "item2"))
                                .setWorkingGroupsInterests(Arrays.asList("oniro"))
                                .setPicture("pic URL")
                                .build(),
                                AccountsProfileData
                                        .builder()
                                        .setUid("42")
                                        .setMail("email@website.com")
                                        .setName("firstlast")
                                        .setFirstName("first")
                                        .setLastName("last")
                                        .setFullName("first last")
                                        .setTwitterHandle("twit")
                                        .setOrg("IBM")
                                        .setJobTitle("Worker")
                                        .setWebsite("google.com")
                                        .setCountry(Country.builder().setCode("US").setName("United States").build())
                                        .setBio("Likes do do things")
                                        .setInterests(Arrays.asList("item1", "item2"))
                                        .setPicture("pic URL")
                                        .build(),
                                AccountsProfileData
                                        .builder()
                                        .setUid("77")
                                        .setMail("email@unreal.com")
                                        .setName("fakeperson")
                                        .setFirstName("fake")
                                        .setLastName("person")
                                        .setFullName("fake person")
                                        .setTwitterHandle("tweets")
                                        .setOrg("Eclipse")
                                        .setJobTitle("Worker")
                                        .setWebsite("google.com")
                                        .setCountry(Country.builder().setCode("CA").setName("Canada").build())
                                        .setBio("Likes do do things")
                                        .setInterests(Arrays.asList("item1", "item2"))
                                        .setPicture("pic URL")
                                        .build(),
                                AccountsProfileData
                                        .builder()
                                        .setUid("88")
                                        .setMail("barshallblathers@email.com")
                                        .setName("barshallblathers")
                                        .setFirstName("barshall")
                                        .setLastName("blathers")
                                        .setFullName("barshall blathers")
                                        .setTwitterHandle("twitter@handle")
                                        .setOrg("Microsoft")
                                        .setJobTitle("Worker")
                                        .setWebsite("google.com")
                                        .setCountry(Country.builder().setCode("UK").setName("United Kingdom").build())
                                        .setBio("Likes do do things")
                                        .setInterests(Arrays.asList("item1", "item2"))
                                        .setPicture("pic URL")
                                        .build()));
    }

    @Override
    public AccountsProfileData getAccountsProfileByUsername(String username, String bearerToken) {
        Optional<AccountsProfileData> result = users.stream().filter(u -> u.getName().equalsIgnoreCase(username)).findFirst();
        if (result.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return result.get();
    }

    @Override
    public List<AccountsProfileData> getAccountsProfileByUid(String uid, String bearerToken) {
        List<AccountsProfileData> results = users.stream().filter(u -> u.getUid().equalsIgnoreCase(uid)).collect(Collectors.toList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }
}
