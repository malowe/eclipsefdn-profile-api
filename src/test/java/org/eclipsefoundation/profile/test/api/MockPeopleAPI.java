/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleEmailsData;
import org.eclipsefoundation.foundationdb.client.model.ProjectData;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.foundationdb.client.model.full.FullPeopleProjectData;
import org.eclipsefoundation.profile.api.PeopleAPI;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;

@Mock
@RestClient
@ApplicationScoped
public class MockPeopleAPI implements PeopleAPI {

    private List<FullPeopleProjectData> peopleProjects;
    private List<PeopleDocumentData> peopleDocs;
    private List<PeopleEmailsData> peopleEmails;
    private List<PeopleData> people;

    public MockPeopleAPI() {

        this.peopleProjects = new ArrayList<>();
        this.peopleProjects
                .addAll(Arrays
                        .asList(createPeopleProject("firstlast", "some.project", "CM"),
                                createPeopleProject("firstlast", "other.project", "ME"),
                                createPeopleProject("username", "some.project", "CM"),
                                createPeopleProject("barshallblathers", "some.project", "ME"),
                                createPeopleProject("username", "other.project", "CM")));

        this.peopleDocs = new ArrayList<>();
        this.peopleDocs
                .addAll(Arrays
                        .asList(PeopleDocumentData
                                .builder()
                                .setPersonID("firstlast")
                                .setDocumentID("ecaId")
                                .setVersion(3.0)
                                .setEffectiveDate(new Date())
                                .build(),
                                PeopleDocumentData
                                        .builder()
                                        .setPersonID("firstlast")
                                        .setDocumentID("iwgpa-id")
                                        .setVersion(1.0)
                                        .setEffectiveDate(new Date())
                                        .build(),
                                PeopleDocumentData
                                        .builder()
                                        .setPersonID("username")
                                        .setDocumentID("openvsx")
                                        .setVersion(1.0)
                                        .setEffectiveDate(new Date())
                                        .build(),
                                PeopleDocumentData
                                        .builder()
                                        .setPersonID("fakeperson")
                                        .setDocumentID("claId")
                                        .setVersion(2.0)
                                        .setEffectiveDate(new Date())
                                        .build(),
                                PeopleDocumentData
                                        .builder()
                                        .setPersonID("barshallblathers")
                                        .setDocumentID("ecaId")
                                        .setVersion(1.0)
                                        .setEffectiveDate(new Date())
                                        .build(),
                                PeopleDocumentData
                                        .builder()
                                        .setPersonID("barshallblathers")
                                        .setDocumentID("icaId")
                                        .setVersion(1.0)
                                        .setEffectiveDate(new Date())
                                        .build()));

        this.peopleEmails = new ArrayList<>();
        this.peopleEmails
                .addAll(Arrays
                        .asList(PeopleEmailsData.builder().setEmailID(1).setPersonID("username").setEmail("fake@mail.org").build(),
                                PeopleEmailsData.builder().setEmailID(2).setPersonID("username").setEmail("also-fake@test.org").build(),
                                PeopleEmailsData.builder().setEmailID(1).setPersonID("firstlast").setEmail("test@false.com").build(),
                                PeopleEmailsData
                                        .builder()
                                        .setEmailID(1)
                                        .setPersonID("barshallblathers")
                                        .setEmail("fake@email.org")
                                        .build()));

        this.people = new ArrayList<>();
        this.people
                .addAll(Arrays
                        .asList(createPerson("username"), createPerson("firstlast"), createPerson("barshallblathers"),
                                createPerson("fakeperson")));
    }

    @Override
    public List<FullPeopleProjectData> getFullPeopleProjects(String personId, boolean isActive) {
        List<FullPeopleProjectData> results = peopleProjects
                .stream()
                .filter(p -> p.getPerson().getPersonID().equalsIgnoreCase(personId))
                .collect(Collectors.toList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<PeopleDocumentData> getDocuments(String personId, boolean isNotExpired) {
        List<PeopleDocumentData> results = peopleDocs
                .stream()
                .filter(p -> p.getPersonID().equalsIgnoreCase(personId))
                .collect(Collectors.toList());
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<PeopleEmailsData> getEmails(String personId) {
        List<PeopleEmailsData> results = peopleEmails.stream().filter(e -> e.getPersonID().equalsIgnoreCase(personId)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public PeopleData getPerson(String personId) {
        List<PeopleData> results = people.stream().filter(e -> e.getPersonID().equalsIgnoreCase(personId)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results.get(0);
    }

    private FullPeopleProjectData createPeopleProject(String personId, String projectId, String relation) {
        return FullPeopleProjectData
                .builder()
                .setPerson(createPerson(personId))
                .setProject(ProjectData
                        .builder()
                        .setProjectID(projectId)
                        .setName("project name")
                        .setLevel(2)
                        .setActive(true)
                        .setParentProjectID("some.id")
                        .setDescription("desc....")
                        .setUrlDownload("download here")
                        .setUrlIndex("index")
                        .setSortOrder(0)
                        .setDiskQuotaGB(0)
                        .setComponent(false)
                        .setStandard(true)
                        .build())
                .setRelation(
                        SysRelationData.builder().setRelation(relation).setDescription("some role").setActive(true).setType("PR").build())
                .setActiveDate(new Date())
                .setEditBugs(false)
                .build();
    }

    private PeopleData createPerson(String personId) {
        return PeopleData
                .builder()
                .setPersonID(personId)
                .setFname("first")
                .setLname("last")
                .setType("XX")
                .setEmail("null")
                .setMember(true)
                .setUnixAcctCreated(true)
                .setIssuesPending(false)
                .setComments("alt-email:testing.email@google.com\r\nalt-email:" + personId + "@test.com\r\n")
                .build();
    }
}
