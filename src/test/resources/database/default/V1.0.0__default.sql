CREATE TABLE `api_eclipse_api_gerrit_review_count` (
  `uid` int NOT NULL DEFAULT 0,
  review_count int NOT NULL DEFAULT 0,
  report_date int NOT NULL DEFAULT 0,
  PRIMARY KEY (`uid`)
);

CREATE TABLE `api_eclipse_api_user_delete_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int NOT NULL DEFAULT 0,
  `name` varchar(60) NOT NULL,
  `mail` varchar(254) NOT NULL,
  `host` varchar(2048) NOT NULL,
  `status` int NOT NULL DEFAULT 0,
  `created` int NOT NULL DEFAULT 0,
  `changed` int NOT NULL DEFAULT 0,
  PRIMARY KEY(`id`)
);

INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'accounts.eclipse.org', 0, 134567891, 134567891);
INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'eclipsecon.org', 0, 134567891, 134567891);
INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'projects.eclipse.org', 0, 134567891, 134567891);
INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'marketplace.eclipse.org', 0, 134567891, 134567891);
INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'blogs.eclipse.org', 0, 134567891, 134567891);
INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'eclipse.org/downloads/packages', 0, 134567891, 134567891);
INSERT INTO api_eclipse_api_user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'newsroom.eclipse.org', 0, 134567891, 134567891);