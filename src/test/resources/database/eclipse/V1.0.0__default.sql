CREATE TABLE `friends` (
  friend_id int NOT NULL DEFAULT 0,
  bugzilla_id int DEFAULT NULL,
  date_joined datetime NOT NULL,
  is_anonymous int NOT NULL DEFAULT 0,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  is_benefit int NOT NULL DEFAULT 0,
  `uid` varchar(60) DEFAULT NULL
);
INSERT INTO friends(friend_id, bugzilla_id, date_joined, is_anonymous, first_name, last_name, is_benefit, `uid`)
VALUES(69, 666, '2022-06-01', 1, 'first', 'last', 1, 'firstlast');
INSERT INTO friends(friend_id, bugzilla_id, date_joined, is_anonymous, first_name, last_name, is_benefit, `uid`)
VALUES(70, 124, '2022-06-01', 1, 'fake', 'person', 1, 'fakeperson');

CREATE TABLE `account_requests` (
  `email` varchar(100) NOT NULL,
  `new_email` varchar(100) default NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `ip` char(15) NOT NULL,
  `req_when` datetime default NULL,
  `token` char(64) default NULL,
  `person_id` varchar(255) default NULL
);